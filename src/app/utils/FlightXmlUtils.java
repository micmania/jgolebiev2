package app.utils;

import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import app.models.Flight;
import app.models.FlightXml;

public class FlightXmlUtils {
    static public void saveFlightsToFile(List<Flight> flights) {
    	saveFlightsToFile(flights,"/data/flights.xml");
    }
    
    static public void saveFlightsToFile(List<Flight> flights, String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(FlightXml.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our person data.
            FlightXml wrapper = new FlightXml();
            wrapper.setFlights(flights);

            //set file to save
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
     
            // Marshalling and saving XML to the file.
            m.marshal(wrapper, file);
        } catch (Exception e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
        }
    }

    static public List<Flight> loadFlightsFromFile() {
        return loadFlightsFromFile("/data/flights.xml");
    }
    
    static public List<Flight> loadFlightsFromFile(String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(FlightXml.class);
            Unmarshaller um = context.createUnmarshaller();

            //set file to load
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
            // Reading XML from the file and unmarshalling.
            if(file.exists()) {
                FlightXml wrapper = (FlightXml) um.unmarshal(file);
                return wrapper.getFlights();
            }else 
                return null;
        } catch (Exception e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
            return null;
        }
    }  
}
