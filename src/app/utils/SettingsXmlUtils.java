package app.utils;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import app.models.Settings;
import app.models.SettingsXml;

public class SettingsXmlUtils {
	/**
	 * 
	 * @param settings
	 */
    static public void saveSettingsToFile(Settings settings) {
    	saveSettingsToFile(settings,"/settings.xml");
    }
	/**
	 * 
	 * @param settings
	 */
    static public void saveSettingsToFile(Settings settings, String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(SettingsXml.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our person data.
            SettingsXml wrapper = new SettingsXml();
            Settings tempSettings = new Settings();
            //encrypt all settings data
            tempSettings.setFtpAddress(StringEncryptionUtils.encrypt(settings.getFtpAddress()));
            tempSettings.setFtpLogin(StringEncryptionUtils.encrypt(settings.getFtpLogin()));
            tempSettings.setFtpPassword(StringEncryptionUtils.encrypt(settings.getFtpPassword()));
            tempSettings.setPigeonsDirectory(StringEncryptionUtils.encrypt(settings.getPigeonsDirectory()));
            tempSettings.setFlightsDirectory(StringEncryptionUtils.encrypt(settings.getFlightsDirectory()));
            tempSettings.setPigeonHouseGeoLocation(StringEncryptionUtils.encrypt(settings.getPigeonHouseGeoLocation()));
            tempSettings.setBeacons(settings.getBeacons());
           
            wrapper.setSettings(tempSettings);

            //set file to save
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
     
            // Marshalling and saving XML to the file.
            m.marshal(wrapper, file);
        } catch (JAXBException e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
        }
    }    
    

    /**
     * 
     * @return
     */
    static public Settings loadSettingsFromFile() {
    	return loadSettingsFromFile("/settings.xml");
    }
    /**
     * 
     * @return
     */
    static public Settings loadSettingsFromFile(String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(SettingsXml.class);
            Unmarshaller um = context.createUnmarshaller();

            //set file to load
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
            // Reading XML from the file and unmarshalling.
            if(file.exists()) {
                SettingsXml wrapper = (SettingsXml) um.unmarshal(file);
                Settings settings = wrapper.getSettings();
                settings.setFtpAddress(StringEncryptionUtils.decrypt(settings.getFtpAddress()));
                settings.setFtpLogin(StringEncryptionUtils.decrypt(settings.getFtpLogin()));
                settings.setFtpPassword(StringEncryptionUtils.decrypt(settings.getFtpPassword()));
                settings.setPigeonsDirectory(StringEncryptionUtils.decrypt(settings.getPigeonsDirectory()));
                settings.setFlightsDirectory(StringEncryptionUtils.decrypt(settings.getFlightsDirectory()));
                settings.setPigeonHouseGeoLocation(StringEncryptionUtils.decrypt(settings.getPigeonHouseGeoLocation()));
                settings.setBeacons(settings.getBeacons());
                
                return settings;
            }else 
                return null;
        } catch (JAXBException e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
            return null;
        }
    }    
}
