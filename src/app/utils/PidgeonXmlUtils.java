/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.utils;

import app.models.Pidgeon;
import app.models.PidgeonXml;
import java.io.File;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Michal
 */
public class PidgeonXmlUtils {

    static public void savePigeonsToFile(List<Pidgeon> pigeons) {
    	savePigeonsToFile(pigeons,"/data/pigeons.xml");
    }
    
    static public void savePigeonsToFile(List<Pidgeon> pigeons, String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(PidgeonXml.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Wrapping our person data.
            PidgeonXml wrapper = new PidgeonXml();
            wrapper.setPidgeons(pigeons);

            //set file to save
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
     
            // Marshalling and saving XML to the file.
            m.marshal(wrapper, file);
        } catch (JAXBException e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
        }
    }

    static public List<Pidgeon> loadPigeonsFromFile() {
        return loadPigeonsFromFile("/data/pigeons.xml");
    }
    
    static public List<Pidgeon> loadPigeonsFromFile(String filePath) {
        try {
            JAXBContext context = JAXBContext.newInstance(PidgeonXml.class);
            Unmarshaller um = context.createUnmarshaller();

            //set file to load
            String appDir = System.getProperty("user.dir");
            File file = new File(appDir + filePath);
            // Reading XML from the file and unmarshalling.
            if(file.exists()) {
                PidgeonXml wrapper = (PidgeonXml) um.unmarshal(file);
                return wrapper.getPidgeons();
            }else 
                return null;
        } catch (JAXBException e) { // catches ANY exception
            ExceptionUtils.showExceptionDialog(e);
            return null;
        }
    }    
}
