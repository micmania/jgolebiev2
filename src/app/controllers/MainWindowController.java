/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javafx.util.Pair;
import jssc.SerialPortException;
import jssc.SerialPortList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.Notifications;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;
import org.controlsfx.glyphfont.GlyphFont;
import org.controlsfx.glyphfont.GlyphFontRegistry;

import app.connector.ftp.FtpFileUploader;
import app.connector.serial.SerialPortReadType;
import app.connector.serial.SerialPortReader;
import app.connector.serial.SerialPortState;
import app.main.JGolebieV2;
import app.models.Flight;
import app.models.FlightPigeon;
import app.models.Log;
import app.models.Pidgeon;
import app.models.Settings;
import app.utils.ExceptionUtils;
import app.utils.FlightXmlUtils;
import app.utils.PidgeonXmlUtils;
import app.utils.SettingsXmlUtils;

/**
 *
 * @author U539509
 */
public class MainWindowController implements Initializable {
	
	private static final Logger log = LogManager.getLogger(MainWindowController.class.getName()); 


	//main window controls
	@FXML
	Button settingsButton;
	@FXML
	Button beaconConnectButton;
	@FXML
	Button beaconDisconnectButton;
	@FXML
	Button pidgeonRemoveButton;
	@FXML
	Button pidgeonEditButton;
	@FXML
	Button pidgeonAddButton;
	@FXML
	Button setPidgeonRing;
	@FXML
	Button sendToServerButton;
	@FXML
	ComboBox<String> beaconCombo;
	@FXML
	ProgressBar uploadProgressBar;
	
	//pigeons table controls
	@FXML
	TableColumn<Pidgeon, Integer> tableColId;
	@FXML
	TableColumn<Pidgeon, String> tableColNr;
	@FXML
	TableColumn<Pidgeon, String> tableColTeam;
	@FXML
	TableColumn<Pidgeon, String> tableColRing;
	@FXML
	TableView<Pidgeon> pidgeonTable;
	@FXML
	TextField ringSearch;
	@FXML
	TextField pigeonNumberSearch;
	@FXML
	TextField teamSearch;
	@FXML
	Button clearPigeonFilter;
	@FXML
	Button removeRingButton;
	
	//flights table controls
	@FXML
	TableColumn<Flight,Integer> tableColFlightId;
	@FXML
	TableColumn<Flight,String> tableColFlightName;
	@FXML
	TableColumn<Flight,String> tableColFlightPlace;
	@FXML
	TableColumn<Flight,String> tableColFlightDistance;
	@FXML
	TableColumn<Flight,String> tableColFlightDate;
	@FXML
	TableColumn<Flight,String> tableColFlightHour;
	@FXML
	TableView<Flight> flightTable;
	@FXML
	TextField nameSearch;
	@FXML
	TextField placeSearch;
	@FXML
	TextField distanceSearch;
	@FXML
	DatePicker dateSearch;
	@FXML
	TextField hourSearch;
	@FXML
	Button clearFlightFilter;
	@FXML
	Button flightAddButton;
	@FXML
	Button flightDeleteButton;
	@FXML
	Button flightEditButton;
	@FXML
	Button flightMapButton;
	@FXML
	Button basketButton;
	
	//logs table controls
	@FXML
	TableColumn<Log, String> logTableDateCol;
	@FXML
	TableColumn<Log, String> logTableTypeCol;
	@FXML
	TableColumn<Log, String> logTableMessageCol;
	@FXML
	TableView<Log> logTable;	
	
	private final ObservableList<Pidgeon> pidgeons = FXCollections.observableArrayList();
	private SortedList<Pidgeon> sortedPidgeons = null;
	private final ObservableList<Flight> flights = FXCollections.observableArrayList();
	private final ObservableList<Log> logs = FXCollections.observableArrayList();
	private List<String> teamsList = new ArrayList<>();
	private SerialPortReader serialPort = null;
	Stage mainWindowStage;

	public void setMainWindowStage(Stage mainWindowStage) {
		this.mainWindowStage = mainWindowStage;
		this.mainWindowStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			@Override
			public void handle(WindowEvent arg0) {
				//handle window close
				if(serialPort != null) {
					try {
						serialPort.close();
					} catch (SerialPortException e) {
						ExceptionUtils.showExceptionDialog(e);
					}
					serialPort = null;
				}
				log.info("Application closed");
			}
		});
	}

	@FXML
	private void onPidgeonAddButtonClicked() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/app/views/PidgeonWindow.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();;
			dialogStage.setTitle("Dodaj gołębia");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainWindowStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);

			PidgeonWindowController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setTeamsList(teamsList);
			//set pidgeons id from 1 to n
			controller.setId(pidgeons.size()+1);
			dialogStage.showAndWait();
			Pidgeon newPidgeon = controller.getPidgeon();
			if (newPidgeon != null) {
				pidgeons.add(newPidgeon);
				// save new list to xml file
				PidgeonXmlUtils.savePigeonsToFile(pidgeons);
				// add team to autocomplete list
				if (!teamsList.contains(newPidgeon.getTeam())) {
					teamsList.add(newPidgeon.getTeam());
				}
			}
			pidgeonTable.getSelectionModel().select(newPidgeon);
			pidgeonTable.scrollTo(newPidgeon);
			pidgeonTable.requestFocus();
			updatePigeonCount();
		} catch (IOException e) {
			// TODO: Show error dialog with stack trace
			e.printStackTrace();
		}
	};

	@FXML
	private void onPidgeonEditButtonClicked() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(
					"/app/views/PidgeonWindow.fxml"));
			AnchorPane page = (AnchorPane) loader.load();

			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edytuj gołębia");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainWindowStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);

			PidgeonWindowController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			Pidgeon selectedPigeon = pidgeonTable.getSelectionModel().getSelectedItem();
			controller.setPidgeon(selectedPigeon);
			controller.setTeamsList(teamsList);
			dialogStage.showAndWait();
			Pidgeon editedPidgeon = controller.getPidgeon();
			if (editedPidgeon != null) {
				int selectedPigeonPos = pidgeons.indexOf(selectedPigeon);
				pidgeons.set(selectedPigeonPos, editedPidgeon);
				// save new list to xml file
				PidgeonXmlUtils.savePigeonsToFile(pidgeons);
				// create new teams list for autocompletion
				teamsList.clear();
				for (Pidgeon pidgeon : pidgeons) {
					String team = pidgeon.getTeam();
					if (!teamsList.contains(team)) {
						teamsList.add(team);
					}
				}
				pidgeonTable.getSelectionModel().select(editedPidgeon);
				pidgeonTable.requestFocus();
				updatePigeonCount();
			}

		} catch (IOException e) {
			// TODO: Show error dialog with stack trace
			e.printStackTrace();
		}
	}

	@FXML
	private void onPidgeonDeleteButtonClicked() {		
		Pidgeon selectedPigeon = pidgeonTable.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
		alert.setTitle("Potwierdzenie");
		alert.setHeaderText("Czy napewno usunąć zaznaczonego gołębia ?");
		alert.setContentText("Id: " + String.valueOf(selectedPigeon.getId()) + "\n" +
							 "Numer: " + selectedPigeon.getNumber() + "\n" + 
							 "Drużyna: " + selectedPigeon.getTeam() + "\n");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			if (selectedPigeon != null) {
				try {
					pidgeons.remove(selectedPigeon);
					// reorder pidgeon id's, set id's from 1 to n
					for (int i = 0; i < pidgeons.size(); i++) {
						pidgeons.get(i).setId(i+1);
					}
					// save new list to xml file
					PidgeonXmlUtils.savePigeonsToFile(pidgeons);
					// create new teams list for autocompletion
					teamsList.clear();
					for (Pidgeon pidgeon : pidgeons) {
						String team = pidgeon.getTeam();
						if (!teamsList.contains(team)) {
							teamsList.add(team);
						}
					}
					updatePigeonCount();
				}catch(Exception e) {
					ExceptionUtils.showExceptionDialog(e);
				}
			}
		}
		pidgeonTable.requestFocus();
	}
	
	private void updatePigeonCount() {
		tableColId.setText("Id (" + pidgeons.size() + ")");
	}
	
	@FXML
	private void onRemoveRingButtonClicked() {
		Pidgeon selectedPigeon = pidgeonTable.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
		alert.setTitle("Potwierdzenie");
		alert.setHeaderText("Czy napewno usunąć obrączke z wybranego gołębia ?");
		alert.setContentText("Id: " + String.valueOf(selectedPigeon.getId()) + "\n" +
							 "Numer: " + selectedPigeon.getNumber() + "\n" + 
							 "Drużyna: " + selectedPigeon.getTeam() + "\n");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			int index = pidgeons.indexOf(selectedPigeon);
			pidgeons.get(index).setRing("");
			PidgeonXmlUtils.savePigeonsToFile(pidgeons);
			//disable ring remove button
			removeRingButton.disableProperty().set(true);
			pidgeonTable.requestFocus();
		}
	}
	
	@FXML
	private void onSetPigeonRingButtonClicked() {
		if(JGolebieV2.settings.getBeacons().size() > 0){
			if (serialPort != null) {
				if (serialPort.isConnected()) {
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Przypisz obrączke");
					alert.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
					alert.setHeaderText("W celu przypisania obrączki do gołębia, przyłóż ją do jednej z anten");
					ProgressBar progress = new ProgressBar();
					Label text = new Label("");
					VBox box = new VBox();
					box.getChildren().add(text);
					box.getChildren().add(progress);
					box.fillWidthProperty().setValue(true);
					box.setSpacing(10);
					alert.getDialogPane().setContent(box);
					alert.getDialogPane().setPrefWidth(500);
					progress.setPrefWidth(500);
					
					Node okButton = alert.getDialogPane().lookupButton(alert.getButtonTypes().get(0));
					okButton.setDisable(true);
					
					ChangeListener<SerialPortState> serialPortStateListener = new ChangeListener<SerialPortState>() {
						@Override
						public void changed(
								ObservableValue<? extends SerialPortState> observable,SerialPortState oldValue,SerialPortState newValue) {
							if(newValue == SerialPortState.READ_STARTED) {
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										text.setText("Odczytuje dane z anteny: " + JGolebieV2.settings.getBeacons().get(serialPort.beaconIndexProperty().getValue()));
										int beaconCount = JGolebieV2.settings.getBeacons().size();
										int currentBeacon = serialPort.beaconIndexProperty().getValue() + 1;
										double prog = (double)currentBeacon / (double)beaconCount;
										progress.progressProperty().setValue(prog);
									}
								});
							}
							else if(newValue == SerialPortState.READ_FINISHED) {
								//read finished with data
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										//check if readed ring is not already assigned
										String data = serialPort.dataProperty().getValue();
										String beacon = data.split(";")[0];
										String ring = data.split(";")[1];
										
										addLogMessage("OK", "Antena " + beacon + ", odczytano obrączke: " + ring);
										log.info(data);
										
										boolean ringAssigned = false;
										for(int i=0; i<pidgeons.size(); i++) {
											if(pidgeons.get(i).getRing().equals(ring)) {
												ringAssigned = true;
												break;
											}
										}
										if(!ringAssigned) {
											alert.setAlertType(AlertType.INFORMATION);
											alert.setHeaderText("Zakończono odczyt");
											text.setText("Odczytano obrączke o numerze: " + ring);
											progress.visibleProperty().setValue(false);
											okButton.setDisable(false);						
											//set pidgeon ring number
											Pidgeon pidgeon = pidgeonTable.getSelectionModel().getSelectedItem();
											pidgeons.get(pidgeons.indexOf(pidgeon)).setRing(ring);
											PidgeonXmlUtils.savePigeonsToFile(pidgeons);
										}else {
											alert.setAlertType(AlertType.WARNING);
											alert.setHeaderText("Zakończono odczyt");
											text.setText("Obrączka o numerze: " + ring +" , jest już przypisana do innego gołębia");
											progress.visibleProperty().setValue(false);
											okButton.setDisable(false);
										}
									}
								});
							}
						}
					};
					serialPort.stateProperty().addListener(serialPortStateListener);
					ChangeListener<SerialPortReadType> serialPortReadTypeListener = new ChangeListener<SerialPortReadType>() {
						@Override
						public void changed(
								ObservableValue<? extends SerialPortReadType> arg0,SerialPortReadType oldValue, SerialPortReadType newValue) {
							if(newValue == SerialPortReadType.DEFAULT) {
								//read operation finished, no data was received from beacons
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										alert.setAlertType(AlertType.ERROR);
										alert.setHeaderText("Zakończono odczyt");
										text.setText("Nie wykryto obrączek w pamięci wybranych anten.");
										progress.visibleProperty().setValue(false);		
										okButton.setDisable(false);
									}
								});
							}
						}
					};
					serialPort.readTypeProperty().addListener(serialPortReadTypeListener);
					serialPort.readDataFromAllBeacons();
					alert.showAndWait();
					//clear listeners
					serialPort.stateProperty().removeListener(serialPortStateListener);
					serialPort.readTypeProperty().removeListener(serialPortReadTypeListener);
				} else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Błąd");
					alert.setHeaderText("Brak połączenia z anteną\nProszę wybrać odpowiedni port i nawiązać połączenie");
					alert.showAndWait();
				}
			}else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Błąd");
				alert.setHeaderText("Brak połączenia z anteną\nProszę wybrać odpowiedni port i nawiązać połączenie");
				alert.showAndWait();
			}
		}else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Błąd");
			alert.setHeaderText("Lista zapisanych anten jest pusta.\nW celu dodania anten, proszę przejść do ustawień programu");
			alert.showAndWait();	
		}
	}
	
	@FXML
	private void onClearPigeonFilterClicked() {
		ringSearch.clear();
		pigeonNumberSearch.clear();
		teamSearch.clear();
	}
	
	@FXML
	private void onFlightAddButtonClicked() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/app/views/FlightWindow.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
	
			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Dodaj lot");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainWindowStage);
			Scene scene = new Scene(page);
			scene.getStylesheets().add(getClass().getResource("/fontSize.css").toString());
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);
			
			FlightWindowController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			controller.setId(flights.size()+1);
			
			dialogStage.showAndWait();
			
			if(controller.isConfirmed()) {
				Flight newFlight = controller.getFlight();
				if(newFlight != null) {
					flights.add(newFlight);
					FlightXmlUtils.saveFlightsToFile(flights);
					flightTable.getSelectionModel().select(newFlight);
					flightTable.scrollTo(newFlight);
					flightTable.requestFocus();
					updateFlightCount();
				}
			}
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML
	private void onFlightDeleteButtonClicked() {
		Flight selectedFlight = flightTable.getSelectionModel().getSelectedItem();
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
		alert.setTitle("Potwierdzenie");
		alert.setHeaderText("Czy napewno usunąć zaznaczony lot ?");
		alert.setContentText("Id: " + String.valueOf(selectedFlight.getId()) + "\n" +
							 "Nazwa: " + selectedFlight.getName() + "\n" + 
							 "Miejsce wypuszczenia: " + selectedFlight.getPlace() + "\n");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			if (selectedFlight != null) {
				try {
					flights.remove(selectedFlight);
					//reorder flight id's
					for (int i = 0; i < flights.size(); i++) {
						flights.get(i).setId(i+1);
					}
					// save new list to xml file
					FlightXmlUtils.saveFlightsToFile(flights);
					updateFlightCount();
				}catch(Exception e) {
					ExceptionUtils.showExceptionDialog(e);
				}
			}
		}
		flightTable.requestFocus();
	}
	
	@FXML
	private void onFlightEditButtonClicked() {
		try {
			// Load the fxml file and create a new stage for the popup dialog.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("/app/views/FlightWindow.fxml"));
			AnchorPane page = (AnchorPane) loader.load();
	
			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Edytuj lot");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainWindowStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
			dialogStage.setResizable(false);
			
			FlightWindowController controller = loader.getController();
			controller.setDialogStage(dialogStage);
			Flight selectedFlight = flightTable.getSelectionModel().getSelectedItem();
			controller.setFlight(selectedFlight);
			
			dialogStage.showAndWait();
			
			if(controller.isConfirmed()) {
				Flight newFlight = controller.getFlight();
				if(newFlight != null) {
					int selectedFlightPos = flights.indexOf(selectedFlight);
					flights.set(selectedFlightPos, newFlight);
					FlightXmlUtils.saveFlightsToFile(flights);
					
					flightTable.getSelectionModel().select(newFlight);
					flightTable.requestFocus();
					updateFlightCount();
				}
			}
		}
		catch(IOException e) {
			ExceptionUtils.showExceptionDialog(e);
		}
	}
	
	@FXML 
	private void onFlightMapButtonClicked() {
		// Load the fxml file and create a new stage for the popup dialog.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/app/views/MapWindow.fxml"));
		try {
			AnchorPane page = (AnchorPane) loader.load();
			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Trasa lotu");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(mainWindowStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
	
			MapWindowController controller = loader.getController();
			controller.setStage(dialogStage);
			if(!JGolebieV2.settings.getPigeonHouseGeoLocation().isEmpty()) {
				controller.setEndLocation(JGolebieV2.settings.getPigeonHouseGeoLocation());
			}
			Flight selectedFlight = flightTable.getSelectionModel().getSelectedItem();
			controller.setStartLocation(selectedFlight.getPlace());
			controller.setReadOnly(true);
			dialogStage.showAndWait();   
		} catch (IOException e) {
			e.printStackTrace();
		}			
	}
	
	private void updateFlightCount() {
		tableColFlightId.setText("Id (" + flights.size() + ")");
	}
	
	@FXML
	private void onBasketButtonClicked() {
		if(JGolebieV2.settings.getBeacons().size() > 0){
			if (serialPort != null) {
				if (serialPort.isConnected()) {
					// Load the fxml file and create a new stage for the popup dialog.
					FXMLLoader loader = new FXMLLoader();
					loader.setLocation(getClass().getResource("/app/views/PigeonsForFlightWindow.fxml"));
					try {
						AnchorPane page = (AnchorPane) loader.load();
						// Create the dialog Stage.
						Stage dialogStage = new Stage();
						dialogStage.setTitle("Koszowanie - " + flightTable.getSelectionModel().getSelectedItem().getName() + " / " +
						flightTable.getSelectionModel().getSelectedItem().getDistance() + " km");
						dialogStage.initModality(Modality.WINDOW_MODAL);
						dialogStage.initOwner(mainWindowStage);
						Scene scene = new Scene(page);
						scene.getStylesheets().add(getClass().getResource("/fontSize.css").toString());
						dialogStage.setScene(scene);
						
						PigeonsForFlightWindowController controller = loader.getController();
						Flight selectedFlight = flightTable.getSelectionModel().getSelectedItem();
						controller.setSerialPort(serialPort);
						controller.setFlight(selectedFlight);
						controller.setPigeons(pidgeons);
				
						dialogStage.showAndWait();
						int index = flights.indexOf(selectedFlight);
						flights.get(index).setPigeonsSaved(controller.getSavedPigeons());
						flights.get(index).setPigeonsNotSaved(controller.getNotSavedPigeons());
						FlightXmlUtils.saveFlightsToFile(flights);						
					} catch (IOException e) {
						log.error(e);						
						ExceptionUtils.showExceptionDialog(e);
					}			
				}else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Błąd");
					alert.setHeaderText("Brak połączenia z anteną\nProszę wybrać odpowiedni port i nawiązać połączenie");
					alert.showAndWait();
				}
			}else {
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Błąd");
				alert.setHeaderText("Brak połączenia z anteną\nProszę wybrać odpowiedni port i nawiązać połączenie");
				alert.showAndWait();
			}
		}else {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Błąd");
			alert.setHeaderText("Lista zapisanych anten jest pusta.\nW celu dodania anten, proszę przejść do ustawień programu");
			alert.showAndWait();	
		}
	}
	
	@FXML
	private void onBeaconConnectButtonClicked() {
		serialPort = new SerialPortReader(beaconCombo.getSelectionModel().getSelectedItem());
		serialPort.stateProperty().addListener(new ChangeListener<SerialPortState>() {
			@Override
			public void changed(
					ObservableValue<? extends SerialPortState> observable,
					SerialPortState oldValue, SerialPortState newValue) {
				if(newValue == SerialPortState.READ_TIMEOUT) {
					addLogMessage("WARN","Przekroczono limit czasu, brak danych w pamięci anteny " + JGolebieV2.settings.getBeacons().get(serialPort.beaconIndexProperty().getValue()));
				}
				else if(newValue == SerialPortState.READ_FINISHED) {
//					addLogMessage("OK","Odczytano dane z anteny " + JGolebieV2.settings.getBeacons().get(serialPort.beaconIndexProperty().getValue()));
				}
				else if(newValue == SerialPortState.READ_STARTED) {
					addLogMessage("INFO", "Odczytuje dane z anteny " + JGolebieV2.settings.getBeacons().get(serialPort.beaconIndexProperty().getValue()));
				}
			}
		});
		try {
			serialPort.open();
			if(serialPort.isConnected()) {
				beaconConnectButton.disableProperty().set(true);
				beaconDisconnectButton.disableProperty().set(false);
				beaconCombo.disableProperty().set(true);
				addLogMessage("OK", "Nawiązano połączenie z portem szeregowym: " + beaconCombo.getSelectionModel().getSelectedItem());
			}
		} catch(SerialPortException e) {
			ExceptionUtils.showExceptionDialog(e);
		}
	}
	
	@FXML
	private void onBeaconDisconnectButtonClicked() {
		try {
			serialPort.close();
			if(!serialPort.isConnected()) {
				serialPort = null;				
				beaconConnectButton.disableProperty().set(false);
				beaconDisconnectButton.disableProperty().set(true);
				beaconCombo.disableProperty().set(false);
				addLogMessage("INFO", "Zakończono połączenie z portem szeregowym: " + beaconCombo.getSelectionModel().getSelectedItem());			
			}
		}
		catch(SerialPortException e) {
			ExceptionUtils.showExceptionDialog(e);
		}
	}

	@FXML
	private void onSendToServerButtonClicked() {
		addLogMessage("INFO", "Rozpoczęto wysyłanie plików na zewnętrzny serwer");
		sendToServerButton.disableProperty().setValue(true);
		uploadProgressBar.visibleProperty().setValue(true);
    	FtpFileUploader.uploadDataFiles();
	}

	@FXML
	private void onSettingsButtonClicked() {
        try {
        	//first ask for user login
        	// Create the custom dialog.
        	Dialog<Pair<String, String>> dialog = new Dialog<>();
        	dialog.getDialogPane().getScene().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
        	dialog.setTitle("Zaloguj się");
        	dialog.setHeaderText("Aby przejść do okna ustawien, proszę podać login i hasło");

        	// Set the button types.
        	ButtonType loginButtonType = new ButtonType("Ok", ButtonData.OK_DONE);
        	ButtonType cancelButtonType = new ButtonType("Anuluj",ButtonData.CANCEL_CLOSE);
        	dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, cancelButtonType);

        	// Create the username and password labels and fields.
        	GridPane grid = new GridPane();
        	grid.setHgap(10);
        	grid.setVgap(10);
        	grid.setPadding(new Insets(10, 10, 10, 10));

        	TextField username = new TextField();
        	username.setPrefWidth(350);
        	username.setPromptText("Login");
        	PasswordField password = new PasswordField();
        	password.setPromptText("Hasło");

        	grid.add(new Label("Login:"), 0, 0);
        	grid.add(username, 1, 0);
        	grid.add(new Label("Hasło:"), 0, 1);
        	grid.add(password, 1, 1);
        	
        	// Enable/Disable login button depending on whether a username was entered.
        	Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
        	loginButton.setDisable(true);

        	// Do some validation (using the Java 8 lambda syntax).
        	username.textProperty().addListener((observable, oldValue, newValue) -> {
        	    loginButton.setDisable(newValue.trim().isEmpty());
        	});

        	dialog.getDialogPane().setContent(grid);
        	
        	// Request focus on the username field by default.
        	Platform.runLater(() -> username.requestFocus());
        	
        	// Convert the result to a username-password-pair when the login button is clicked.
        	dialog.setResultConverter(dialogButton -> {
        	    if (dialogButton == loginButtonType) {
        	        return new Pair<>(username.getText(), password.getText());
        	    }
        	    return null;
        	});
        	
        	Optional<Pair<String, String>> result = dialog.showAndWait();
        	
        	if(result.isPresent()) {
        		//check for user login and password
        		if(result.get().getKey().toString().equals("user0") && result.get().getValue().toString().equals("user0")) {   		
	                // Load the fxml file and create a new stage for the popup dialog.
	                FXMLLoader loader = new FXMLLoader();
	                loader.setLocation(getClass().getResource("/app/views/SettingsWindow.fxml"));
	                AnchorPane page = (AnchorPane)loader.load();
	
	                // Create the dialog Stage.
	                Stage dialogStage = new Stage();
	                dialogStage.setTitle("Ustawienia");
	                dialogStage.initModality(Modality.WINDOW_MODAL);
	                dialogStage.initOwner(mainWindowStage);
	                Scene scene = new Scene(page);
	                scene.getStylesheets().add(getClass().getResource("/fontSize.css").toString());
	                dialogStage.setScene(scene);
	                dialogStage.setResizable(false);
	
	                SettingsWindowController controller = loader.getController();
	                controller.setDialogStage(dialogStage);
	                dialogStage.showAndWait();
        		}
        		else {
        			//wrong credentials provided, show error dialog
        			Alert alert = new Alert(AlertType.ERROR);
        			alert.setTitle("Błąd");
        			alert.setHeaderText("Podano błędne dane użytkownika");
        			alert.setContentText(null);
        			alert.showAndWait();
        		}
        	}
        }
        catch(IOException e) {
            //TODO: Show error dialog with stack trace
            e.printStackTrace();
        }
	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {
		//load settings from xml file
	    Settings loadedSettings = SettingsXmlUtils.loadSettingsFromFile();
	    if(loadedSettings != null) {
	    	JGolebieV2.settings = loadedSettings;
	    }
    	
    	//connect listeners to ftp file uploader
    	FtpFileUploader.fileTransferStarted.addListener(new ChangeListener<Boolean> () {
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0,
					Boolean oldValue, Boolean newValue) {
				if(newValue == false) {
					//file transfer ended, show send button
					sendToServerButton.disableProperty().setValue(false);
					//hide progress bar
					uploadProgressBar.visibleProperty().setValue(false);
					//show message if errors occured
					if(FtpFileUploader.fileTransferError.getValue() == true) {
						addLogMessage("ERR", "Wysyłanie plików na zewnętrzny serwer nie powiodło się");
						showNotificationError("Wysyłanie plików na zewnętrzny serwer nie powiodło się");
					}
					else 
						addLogMessage("OK", "Wysyłanie plików na zewnętrzny serwer zakończone sukcesem!");
				}
			}
    	});
    	
		// load pigeon data from xml file
		List<Pidgeon> loadedPidgeons = PidgeonXmlUtils.loadPigeonsFromFile();
		if (loadedPidgeons != null) {
			pidgeons.addAll(loadedPidgeons);
			// add all teams to autocomplete list
			for (Pidgeon pidgeon : pidgeons) {
				String team = pidgeon.getTeam();
				if (!teamsList.contains(team)) {
					teamsList.add(team);
				}
			}
		}
		updatePigeonCount();
		
		//load flights from xml files
		List<Flight> loadedFlights = FlightXmlUtils.loadFlightsFromFile();
		if(loadedFlights != null) {
			flights.addAll(loadedFlights);
		}
		updateFlightCount();
		
		//create dummy flights for test
//		for(int i=0; i<150; i++) {
//			flights.add(new Flight(flights.size(),"LOT " + String.valueOf(i),"Gdynia",i,LocalDate.now(),"12:00:00"));
//		}
		
		//create dummy pigeons for test
//		for(int i=0; i<3000; i++) {
//			pidgeons.add(new Pidgeon(pidgeons.size(),"PL-0000-000" + String.valueOf(i),"DRUŻYNA/" + String.valueOf(i),"Samiec","Płowy","N/A","Opis"));
//		}

		// configure pidgeon table columns
		tableColId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
		tableColNr.setCellValueFactory(cellData -> cellData.getValue().numberProperty());
		tableColTeam.setCellValueFactory(cellData -> cellData.getValue().teamProperty());
		tableColRing.setCellValueFactory(cellData -> cellData.getValue().ringProperty());

		pidgeonTable.getSelectionModel().selectedItemProperty().
		addListener((observable, oldValue, newValue) -> pidgeonSelectionChanged(newValue));
		
        FilteredList<Pidgeon> filteredPidgeons = new FilteredList<>(pidgeons, p -> true);
        
        ringSearch.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldValue, String newValue) {
				filteredPidgeons.setPredicate(pidgeon -> {
					if(newValue == null || newValue.isEmpty()) {
						return true;
					}
					String ring = newValue;
					String number = "";
					String team = "";
					if(!pigeonNumberSearch.getText().isEmpty())
						number = pigeonNumberSearch.getText();
					if(!teamSearch.getText().isEmpty()) 
						team = teamSearch.getText(); 
					if(pidgeon.getRing().startsWith(ring) && pidgeon.getNumber().startsWith(number) && pidgeon.getTeam().startsWith(team)) {
						return true;
					}
					return false;
				});
			}
        });
        
        pigeonNumberSearch.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldValue, String newValue) {
				filteredPidgeons.setPredicate(pidgeon -> {
					if(newValue == null || newValue.isEmpty()) {
						return true;
					}
					String ring = "";
					String number = newValue;
					String team = "";
					if(!ringSearch.getText().isEmpty())
						ring = ringSearch.getText();
					if(!teamSearch.getText().isEmpty()) 
						team = teamSearch.getText(); 
					if(pidgeon.getRing().startsWith(ring) && pidgeon.getNumber().startsWith(number) && pidgeon.getTeam().startsWith(team)) {
						return true;
					}
					return false;
				});
			}
        });
        
        teamSearch.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String oldValue, String newValue) {
				filteredPidgeons.setPredicate(pidgeon -> {
					if(newValue == null || newValue.isEmpty()) {
						return true;
					}
					String ring = "";
					String number = "";
					String team = newValue;
					if(!ringSearch.getText().isEmpty())
						ring = ringSearch.getText();
					if(!pigeonNumberSearch.getText().isEmpty()) 
						number = pigeonNumberSearch.getText(); 
					if(pidgeon.getRing().startsWith(ring) && pidgeon.getNumber().startsWith(number) && pidgeon.getTeam().startsWith(team)) {
						return true;
					}
					return false;
				});
			}
        });        
        
        sortedPidgeons = new SortedList<>(filteredPidgeons);
        sortedPidgeons.comparatorProperty().bind(pidgeonTable.comparatorProperty());
		pidgeonTable.setItems(sortedPidgeons);
		
		MenuItem editPigeonItem = new MenuItem("Edytuj");
		editPigeonItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.PENCIL));
		editPigeonItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				onPidgeonEditButtonClicked();
			}
		});
		MenuItem deletePigeonItem = new MenuItem("Usuń");
		deletePigeonItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.TIMES));
		deletePigeonItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				onPidgeonDeleteButtonClicked();
			}
		});
		
		pidgeonTable.setContextMenu(new ContextMenu(editPigeonItem,deletePigeonItem));
				
		//add columns width change listener
		tableColNr.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldValue, Number newValue) {
				double newWidth = newValue.doubleValue() - 5.0;
				pigeonNumberSearch.setMinWidth(newWidth);
				pigeonNumberSearch.setMaxWidth(newWidth);
				pigeonNumberSearch.setPrefWidth(newWidth);
			}
		});
		
		//configure flight table columns
		tableColFlightId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
		tableColFlightName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
		tableColFlightPlace.setCellValueFactory(cellData -> cellData.getValue().placeProperty());
		tableColFlightDistance.setCellValueFactory(cellData -> {
			return new SimpleStringProperty(String.valueOf(cellData.getValue().getDistance() + " km"));
		});
//		tableColFlightDate.setCellValueFactory(cellData -> cellData.getValue().getDate().toString());
		//TODO: add date display
		tableColFlightDate.setCellValueFactory(cellData -> {
			if(cellData.getValue().getDate() != null) {
				SimpleStringProperty dateProperty = new SimpleStringProperty();
		        dateProperty.setValue(cellData.getValue().getDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
				return dateProperty;
			}
			else return new SimpleStringProperty(" ");
							
		});
		tableColFlightHour.setCellValueFactory(cellData -> cellData.getValue().hourProperty());
		
		MenuItem editFlightItem = new MenuItem("Edytuj");
		editFlightItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.PENCIL));
		editFlightItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				onFlightEditButtonClicked();
			}
		});
		MenuItem deleteFlightItem = new MenuItem("Usuń");
		deleteFlightItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.TIMES));
		deleteFlightItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				onFlightDeleteButtonClicked();
			}
		});
		
		MenuItem setBasketingItem = new MenuItem("Koszowanie");
		setBasketingItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.TWITTER));
		setBasketingItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				onBasketButtonClicked();
			}
		});
		
		MenuItem showMapItem = new MenuItem("Mapa");
		showMapItem.setGraphic(GlyphFontRegistry.font("FontAwesome").create(FontAwesome.Glyph.GLOBE));
		showMapItem.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				onFlightMapButtonClicked();
			}
		});
		
		flightTable.setContextMenu(new ContextMenu(editFlightItem,deleteFlightItem,setBasketingItem,showMapItem));
		
		//TODO: add filtering
		flightTable.setItems(flights);
		flightTable.getSelectionModel().selectedItemProperty().
		addListener((observable, oldValue, newValue) -> flightSelectionChanged(newValue));
		
		tableColFlightName.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldValue, Number newValue) {
				double newWidth = newValue.doubleValue();
				nameSearch.setMinWidth(newWidth);
				nameSearch.setMaxWidth(newWidth);
				nameSearch.setPrefWidth(newWidth);
			}
		});	
		tableColFlightPlace.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldValue, Number newValue) {
				double newWidth = newValue.doubleValue() - 5.0;
				placeSearch.setMinWidth(newWidth);
				placeSearch.setMaxWidth(newWidth);
				placeSearch.setPrefWidth(newWidth);
			}
		});	
		tableColFlightDistance.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldValue, Number newValue) {
				double newWidth = newValue.doubleValue() - 5.0;
				distanceSearch.setMinWidth(newWidth);
				distanceSearch.setMaxWidth(newWidth);
				distanceSearch.setPrefWidth(newWidth);
			}
		});
		tableColFlightDate.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> arg0,
					Number oldValue, Number newValue) {
				double newWidth = newValue.doubleValue() - 5.0;
				dateSearch.setMinWidth(newWidth);
				dateSearch.setMaxWidth(newWidth);
				dateSearch.setPrefWidth(newWidth);
			}
		});		
		
		//configure log table columns
		logTableDateCol.setCellValueFactory(cellData -> cellData.getValue().dateProperty());
		logTableTypeCol.setCellValueFactory(cellData -> cellData.getValue().typeProperty());
		// Custom rendering of the table cell.
		logTableTypeCol.setCellFactory(column -> {
		    return new TableCell<Log, String>() {
		    	@Override
		    	protected void updateItem(String item, boolean empty) {
                    super.updateItem(item, empty);
                    if(!empty) {
                    	GlyphFont fontAwesome = GlyphFontRegistry.font("FontAwesome");
                    	setStyle("-fx-alignment: CENTER;");
                    	if(item.equals("INFO")) {
                    		Glyph glyph = fontAwesome.create("QUESTION_CIRCLE");
                    		glyph.setColor(Color.BLUE);
                    		setGraphic(glyph);
                    	}
                    	else if(item.equals("ERR")) { 
                    		Glyph glyph = fontAwesome.create("EXCLAMATION_CIRCLE");
                    		glyph.setColor(Color.RED);
                    		setGraphic(glyph);
                    	}
                    	else if(item.equals("WARN")){ 
                    		Glyph glyph = fontAwesome.create("EXCLAMATION_TRIANGLE");
                    		glyph.setColor(Color.ORANGE);
                    		setGraphic(glyph);                    		                   		
                    	}
                    	else if(item.equals("OK")) {
                    		Glyph glyph = fontAwesome.create("CHECK_CIRCLE");
                    		glyph.setColor(Color.GREEN);
                    		setGraphic(glyph);  
                    	}
                    }
		    	}
		    };
		});
		logTableMessageCol.setCellValueFactory(cellData -> cellData.getValue().messageProperty());
		logTable.setItems(logs);

		// read available serial ports
		beaconCombo.getItems().addAll(SerialPortList.getPortNames());
		// set first found serial port as current
		if (beaconCombo.getItems().size() != 0) {
			beaconCombo.getSelectionModel().select(0);
		} else {
			beaconCombo.disableProperty().set(true);
			beaconConnectButton.disableProperty().set(true);
		}

		// disable buttons
		beaconDisconnectButton.disableProperty().set(true);
		pidgeonRemoveButton.disableProperty().set(true);
		pidgeonEditButton.disableProperty().set(true);
		setPidgeonRing.disableProperty().set(true);
		removeRingButton.disableProperty().set(true);
		flightDeleteButton.disableProperty().set(true);
		flightEditButton.disableProperty().set(true);
		flightMapButton.disableProperty().set(true);
		basketButton.disableProperty().set(true);
		
		//hide file upload progress bar
		uploadProgressBar.visibleProperty().setValue(false);
	};

	/**
	 * disable or enable edit,remove and set ring buttons
	 */
	private void pidgeonSelectionChanged(Pidgeon pidgeon) {
		if (pidgeon != null) {
			pidgeonRemoveButton.disableProperty().set(false);
			pidgeonEditButton.disableProperty().set(false);
			setPidgeonRing.disableProperty().set(false);
			//check if selected pigeon has ring 
			Pidgeon selectedPigeon = pidgeonTable.getSelectionModel().getSelectedItem();
			if(!selectedPigeon.getRing().isEmpty())
				removeRingButton.disableProperty().set(false);
			else {
				removeRingButton.disableProperty().set(true);
			}
		} else {
			pidgeonRemoveButton.disableProperty().set(true);
			pidgeonEditButton.disableProperty().set(true);
			setPidgeonRing.disableProperty().set(true);
			removeRingButton.disableProperty().set(true);
		}
	}
	
	private void flightSelectionChanged(Flight flight) {
		if(flight != null) {
			flightDeleteButton.disableProperty().set(false);
			flightEditButton.disableProperty().set(false);
			flightMapButton.disableProperty().set(false);
			basketButton.disableProperty().set(false);			
		} else {
			flightDeleteButton.disableProperty().set(true);
			flightEditButton.disableProperty().set(true);
			flightMapButton.disableProperty().set(true);
			basketButton.disableProperty().set(true);			
		}
	}
	
	private void addLogMessage(String type, String message) {
		//check messages count, temp resolution. Get value from configuration
		if(logs.size() > 50) 
			logs.clear();
		//create new log message
		Log logMessage = new Log();
		DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss.SSS");
		Date date = new Date();
		dateFormat.format(date);
		logMessage.setDate(dateFormat.format(date));
		logMessage.setType(type);
		logMessage.setMessage(message);
		logs.add(logMessage);
	}
	
	private void showNotificationError(String message) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {	
				Notifications notificationBuilder = Notifications
						.create()
						.title("Błąd / JGolebieV2 / " + beaconCombo.getSelectionModel().getSelectedItem())
						.text(message)
						.hideAfter(Duration.seconds(5))
						.position(Pos.BOTTOM_RIGHT);
				notificationBuilder.showError();				
			}
		});		
	}
	
	@SuppressWarnings("unused")
	private void showNotificationWarning(String message) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {	
				Notifications notificationBuilder = Notifications
						.create()
						.title("Błąd / JGolebieV2 / " + beaconCombo.getSelectionModel().getSelectedItem())
						.text(message)
						.hideAfter(Duration.seconds(5))
						.position(Pos.BOTTOM_RIGHT);
				notificationBuilder.showWarning();				
			}
		});		
	}	
}
