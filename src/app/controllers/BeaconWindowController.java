package app.controllers;

import java.net.URL;
import java.util.ResourceBundle;

import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;
import org.controlsfx.validation.decoration.ValidationDecoration;

import app.models.Beacon;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import jssc.SerialPortList;

public class BeaconWindowController implements Initializable{

	@FXML
	private TextField beaconNumber;
	@FXML
	private ComboBox<String> portCombo;
	@FXML
	private Button okButton;
	@FXML
	private Button cancelButton;
	
    ValidationSupport validation = new ValidationSupport();
    Stage dialogStage = null;
    boolean confirmed = false;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// read available serial ports
		portCombo.getItems().addAll(SerialPortList.getPortNames());
		// set first found serial port as current
		if (portCombo.getItems().size() != 0) {
			portCombo.getSelectionModel().select(0);
		} else {
			portCombo.disableProperty().set(true);
			portCombo.disableProperty().set(true);
		}
		
		//set validation
        validation.setErrorDecorationEnabled(true);
        ValidationDecoration cssDecorator = new StyleClassValidationDecoration();
        validation.setValidationDecorator(cssDecorator);
        validation.registerValidator(beaconNumber, true, Validator.createEmptyValidator("Proszę podać numer anteny"));
        beaconNumber.setText(" ");
        beaconNumber.setText("");
        beaconNumber.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String arg1, String newValue) {
				beaconNumber.setText(newValue.toUpperCase());				
			}
        });
        beaconNumber.requestFocus();
	}
	
	@FXML
	private void onOkButtonClicked() {
		//check data before closing the window
		if(!validation.isInvalid()){
			confirmed = true;
			dialogStage.close();
		}
		else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Błąd");
            alert.setHeaderText("Wprowadzone dane są niekompletne lub mają niepoprawny format");
            String message = "";
            for(ValidationMessage msg: validation.getValidationResult().getMessages()) {
            	message += msg.getText() + "\n";
            }
            alert.setContentText(message);
            alert.showAndWait();		
        }
	}
	
	@FXML
	private void onCancelButtonClicked() {
		confirmed = false;
		dialogStage.close();
	}
	
	public void setDialogStage(Stage stage) {
		dialogStage = stage;
	}
	
	public Beacon getBeacon() {
		if(confirmed) {
			return new Beacon(beaconNumber.getText(),portCombo.getSelectionModel().getSelectedItem());
		} else return null;
	}
	
	public void setBeacon(Beacon beacon) {
		beaconNumber.setText(beacon.getNumber());
		portCombo.getSelectionModel().select(beacon.getPort());
	}
}
