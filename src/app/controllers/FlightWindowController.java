package app.controllers;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;
import org.controlsfx.validation.decoration.ValidationDecoration;

import app.main.JGolebieV2;
import app.models.Flight;

public class FlightWindowController implements Initializable {
	
	@FXML
	Button saveButton;
	@FXML
	Button cancelButton;
	@FXML
	Button showMapButton;
	@FXML
	TextField flightId;
	@FXML
	TextField flightName;
	@FXML
	TextField flightPlace;
	@FXML
	TextField flightDistance;
	@FXML
	DatePicker flightDate;
	@FXML
	TextField flightHour;
	
	private Stage dialogStage = null;
	private boolean confirmed = false;
    ValidationSupport validation = new ValidationSupport();

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		flightDate.setValue(LocalDate.now());
		
        validation.setErrorDecorationEnabled(true);
        ValidationDecoration cssDecorator = new StyleClassValidationDecoration();
        validation.setValidationDecorator(cssDecorator);
        validation.registerValidator(flightName, true, Validator.createEmptyValidator("Proszę podać nazwę lotu"));
        validation.registerValidator(flightPlace, true, Validator.createEmptyValidator("Proszę podać miejsce wypuszczenia"));
        validation.registerValidator(flightDistance, true, Validator.createEmptyValidator("Proszę podać odległość"));
        validation.registerValidator(flightDate, true, Validator.createEmptyValidator("Proszę podać datę lotu"));
        validation.registerValidator(flightHour, true, Validator.createEmptyValidator("Proszę podać godzinę rozpoczęcia lotu"));
        validation.registerValidator(flightHour, Validator.createRegexValidator("Podano zły format godziny", "([01]?[0-9]|2[0-3]):[0-5][0-9]", Severity.ERROR));
        
        //fix for enabling redecoration
        flightName.setText(" ");
        flightName.setText("");
        validation.redecorate();
	}
	
	public void setDialogStage(Stage dialogStage) {
		this.dialogStage = dialogStage;
		dialogStage.sceneProperty().getValue().getStylesheets().add(getClass().getResource("/validation.css").toExternalForm());		
	}
	
	public void setFlight(Flight flight) {
		if(flight != null) {
			flightId.setText(String.valueOf(flight.getId()));
			flightName.setText(flight.getName());
			flightPlace.setText(flight.getPlace());
			flightDistance.setText(String.valueOf(flight.getDistance()));
			flightDate.setValue(flight.getDate());
			flightHour.setText(flight.getHour());
		}
	}
	
	public Flight getFlight() {
		if(confirmed) {
			double distance = Double.valueOf(flightDistance.getText().replace(",", "."));
			return new Flight(Integer.valueOf(flightId.getText()), 
					flightName.getText(),
					flightPlace.getText(),
					distance,
					flightDate.getValue(),
					flightHour.getText());
		}
		else 
			return null;
	}
	
	@FXML
	private void onSaveButtonClicked() {
		//check data before closing the window
		if(!validation.isInvalid()){
			confirmed = true;
			dialogStage.close();
		}
		else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Błąd");
            alert.setHeaderText("Wprowadzone dane są niekompletne lub mają niepoprawny format");
            String message = "";
            for(ValidationMessage msg: validation.getValidationResult().getMessages()) {
            	message += msg.getText() + "\n";
            }
            alert.setContentText(message);
            alert.showAndWait();		
        }
	}

	@FXML
	private void onCancelButtonClicked() {
		confirmed = false;
		dialogStage.close();
	}
	
	@FXML
	private void onShowMapButtonClicked() {
		// Load the fxml file and create a new stage for the popup dialog.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/app/views/MapWindow.fxml"));
		try {
			AnchorPane page = (AnchorPane) loader.load();
			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Wybierz lokalizacje");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(this.dialogStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
	
			MapWindowController controller = loader.getController();
			controller.setStage(dialogStage);
			if(!JGolebieV2.settings.getPigeonHouseGeoLocation().isEmpty()) {
				controller.setEndLocation(JGolebieV2.settings.getPigeonHouseGeoLocation());
			}
			controller.setStartLocation(flightPlace.getText());
			dialogStage.showAndWait();   
			if(controller.isConfirmed()) {
				flightPlace.setText(controller.getStartLocationName());
				flightDistance.setText(String.format("%.2f", controller.getMarkersDistance()));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public boolean isConfirmed() {
		return confirmed;
	}
	
	public void setId(int id) {
		flightId.setText(String.valueOf(id));
	}

}
