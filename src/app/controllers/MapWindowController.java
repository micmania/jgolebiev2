package app.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import app.utils.ExceptionUtils;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Duration;

public class MapWindowController implements Initializable {
	
	@FXML
	BorderPane borderPane;
	@FXML
	TextField mapLocationSearch;
	@FXML
	ComboBox<String> mapType;
	@FXML
	Label mapLocation;
	
	private Stage dialogStage;
	private WebView webView;
	private WebEngine webEngine;
    private Timeline locationUpdateTimeline;
	private String startLocationName = "";
	private String startLocationLatLong = "";
	private String endLocationName = "";
	private String endLocationLatLong = "";
	private double markersDistance = 0.0;
	private boolean confirmed = false;
	private List<String> scriptQueue = new ArrayList<>();
	private boolean pageLoaded = false;

	public MapWindowController() {     		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		//add map widget to window center
        webView = new WebView();
        webEngine = webView.getEngine();//new WebEngine(getClass().getResource("googlemap.html").toString());
        try {
        	webEngine.load(MapWindowController.class.getResource("/googlemap_backup.html").toExternalForm());
        }catch(Exception e) {
        	ExceptionUtils.showExceptionDialog(e);
        }
        
        Label loadingLabel = new Label("Ładowanie...");
        borderPane.setCenter(loadingLabel);
              
        webEngine.setOnAlert((WebEvent<String> wEvent) -> {
        	try {
	        	String rawData = wEvent.getData();
	        	System.out.println(rawData);
	        	String dataType = rawData.split(":")[0];
	        	String data = rawData.split(":")[1];
	        	if(dataType.equals("startMarkerLocation")) {
		            startLocationName =  data;
		            mapLocation.setText(startLocationName);
	        	}
	        	else if(dataType.equals("startMarkerLatLong")) {
	        		startLocationLatLong = data;
	        	}
	        	else if(dataType.equals("endMarkerLocation")) {
	        		
	        	}
	        	else if(dataType.equals("endMarkerLatLong")) {
	        		
	        	}
	        	else if(dataType.equals("markersDistance")) {
	        		markersDistance = Double.valueOf(data);
	        		//convert from m to km
	        		markersDistance = markersDistance/1000;
	        	}
	        	else if(dataType.equals("pageState")) {
	        		pageLoaded = true;
	        		//show loaded map widget
	                borderPane.setCenter(webView);
	        	}
        	}catch(Exception e) {
        		e.printStackTrace();
        	}
        });
        
        webEngine.getLoadWorker().progressProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue) {
				if(newValue.doubleValue() > 0.89 && pageLoaded) { 
					//execute script in queued order FIFO
					while(scriptQueue.size() > 0) {
						System.out.println("EXEC: " + scriptQueue.get(0));
						try {
							webEngine.executeScript(scriptQueue.get(0));
						}catch(Exception e) {
							ExceptionUtils.showExceptionDialog(e);
						}
						scriptQueue.remove(0);
					}
				}
			}
        });
        
        //fill map types combo
        mapType.getItems().addAll("Drogowa","Satelitarna","Hybrydowa","Teren");
        mapType.getSelectionModel().select("Hybrydowa");
        mapType.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue) {
				if(newValue.equals("Drogowa")) {
                    webEngine.executeScript("document.setMapTypeRoad()");					
				}
				else if(newValue.equals("Satelitarna")) {
                    webEngine.executeScript("document.setMapTypeSatellite()");					
				}
				else if(newValue.equals("Hybrydowa")) {
                    webEngine.executeScript("document.setMapTypeHybrid()");					
				}
				else if(newValue.equals("Teren")) {
                    webEngine.executeScript("document.setMapTypeTerrain()");					
				}
				
			}
        });
        
        //add listeners
        mapLocationSearch.textProperty().addListener(new ChangeListener<String>() {
            public void changed(ObservableValue<? extends String> observableValue, String s, String s1) {
                // delay location updates to we don't go too fast file typing
                if (locationUpdateTimeline!=null) locationUpdateTimeline.stop();
                locationUpdateTimeline = new Timeline();
                locationUpdateTimeline.getKeyFrames().add(
                    new KeyFrame(new Duration(400), new EventHandler<ActionEvent>() {
                        public void handle(ActionEvent actionEvent) {
                            webEngine.executeScript("document.goToLocation(\""+mapLocationSearch.getText()+"\")");
                        }
                    })
                );
                locationUpdateTimeline.play();
            }
        });
	}
	
	@FXML
	private void onOkButtonClicked() {
		confirmed = true;
		dialogStage.close();
	}
	
	@FXML
	private void onCancelButtonClicked() {
		confirmed = false;
		dialogStage.close();
	}
	
	public void setStage(Stage stage) {
		dialogStage = stage;
	}
	
	public void setStartLocation(String location) {
		scriptQueue.add("document.goToLocation(\""+location+"\")");			
		scriptQueue.add("document.setStartMarkerLocation(\""+location+"\")");
        startLocationName =  location;
        mapLocation.setText(startLocationName);		
	}
	
	public void setEndLocation(String location) {
		scriptQueue.add("document.setEndMarkerLocation(\""+location+"\")");
        endLocationName =  location;	
	}
	
	public String getStartLocationName() {
		return startLocationName;
	}
	
	public String getStartLocationLatLong() {
		return startLocationLatLong;
	}
	
	public String getEndLocationName() {
		return endLocationName;
	}
	
	public String getEndLocationLatLong() {
		return endLocationLatLong;
	}
	
	public double getMarkersDistance() {
		return markersDistance;
	}
	
	public boolean isConfirmed() {
		return confirmed;
	}
	
	public void setReadOnly(boolean value) {
		if(value) {
			scriptQueue.add("document.setReadOnly(\""+1+"\")");
		}else {
			scriptQueue.add("document.setReadOnly(\""+0+"\")");			
		}

	}

}
