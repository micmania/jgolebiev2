/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import app.models.Pidgeon;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

import org.controlsfx.control.textfield.TextFields;
import org.controlsfx.validation.ValidationMessage;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;
import org.controlsfx.validation.decoration.ValidationDecoration;

/**
 * FXML Controller class
 *
 * @author U539509
 */
public class PidgeonWindowController implements Initializable {
    
    @FXML
    private TextField pidgeonId;
    @FXML
    private TextField pidgeonNumber;
    @FXML
    private TextField pidgeonTeam;
    @FXML
    private ComboBox<String> pidgeonSex;
    @FXML
    private TextField pigeonRing;
    @FXML
    private TextArea pigeonDescription;
    @FXML
    private Button saveButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button setRingButton;
    @FXML
    private ComboBox<String> pigeonColor;
    private boolean confirmed;
    private Stage dialogStage;
    private List<String> currentTeams;
    
    ValidationSupport validation = new ValidationSupport();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        confirmed = false;
        //add validation to required fields
        validation.setErrorDecorationEnabled(true);
        ValidationDecoration cssDecorator = new StyleClassValidationDecoration();
        validation.setValidationDecorator(cssDecorator);
        validation.registerValidator(pidgeonNumber, true, Validator.createEmptyValidator("Proszę podać numer gołebia"));
        validation.registerValidator(pidgeonTeam, true, Validator.createEmptyValidator("Proszę podać drużynę"));
        validation.registerValidator(pidgeonSex, true, Validator.createEmptyValidator("Proszę wybrać płeć"));
        validation.registerValidator(pigeonColor, true, Validator.createEmptyValidator("Proszę wybrać kolor"));
        
        pidgeonSex.getItems().addAll("Młody","Samiec","Samica");
        pidgeonSex.getSelectionModel().select(0);
        pigeonColor.getItems().addAll("Biały","Ciemny","Ciemny pstry","Ciemny nakrapiany","Czarny","Czarny pstry",
        		"Czerwony","Czerwony pstry","Czerwony nakrapiany","Nakrapiany","Nakrapiany pstry","Niebieski","Niebieski pstry",
        		"Niebieski nakrapiany","Płowy","Pstry","Szpak","Inny");
        pigeonColor.getSelectionModel().select(0);
        
        //set pidgeon number to be upper case
        pidgeonNumber.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0,
					String arg1, String newValue) {
				pidgeonNumber.setText(newValue.toUpperCase());				
			}
        });
        validation.redecorate();
    } 
    
    public void setId(int id) {
        pidgeonId.setText(String.valueOf(id));
    }
    
    public void setTeamsList(List<String> teamsList) {
        this.currentTeams = teamsList;
        TextFields.bindAutoCompletion(pidgeonTeam,currentTeams);
    }
    
    @FXML
    private void onOkButtonClicked() {
		//check data before closing the window
		if(!validation.isInvalid()){
			confirmed = true;
			dialogStage.close();
		}
		else {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Błąd");
            alert.setHeaderText("Wprowadzone dane są niekompletne lub mają niepoprawny format");
            String message = "";
            for(ValidationMessage msg: validation.getValidationResult().getMessages()) {
            	message += msg.getText() + "\n";
            }
            alert.setContentText(message);
            alert.showAndWait();		
        }
    }
    
    @FXML
    private void onCancelButtonClicked() {
        confirmed = false;
        dialogStage.close();
    }
    
    public void setDialogStage(Stage stage) {
        dialogStage = stage;
		dialogStage.sceneProperty().getValue().getStylesheets().add(getClass().getResource("/validation.css").toExternalForm());		        
    }
    
    public void setPidgeon(Pidgeon pidgeon) {
        pidgeonId.setText(String.valueOf(pidgeon.getId()));
        pidgeonNumber.setText(pidgeon.getNumber());
        pidgeonTeam.setText(pidgeon.getTeam());
        pidgeonSex.getSelectionModel().select(pidgeon.getSex());
        pigeonColor.getSelectionModel().select(pidgeon.getColor());
        pigeonDescription.setText(pidgeon.getDescription());
        pigeonRing.setText(pidgeon.getRing());
    };
    
    public Pidgeon getPidgeon() {
        if(confirmed) {
            return new Pidgeon(Integer.valueOf(pidgeonId.getText()),
                    pidgeonNumber.getText(),
                    pidgeonTeam.getText(),
                    pidgeonSex.getSelectionModel().getSelectedItem().toString(),
                    pigeonColor.getSelectionModel().getSelectedItem().toString(),
                    pigeonRing.getText(),
                    pigeonDescription.getText()
                    );
        }
        else 
            return null;
    };
        
}
