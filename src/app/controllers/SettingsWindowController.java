/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import app.main.JGolebieV2;
import app.utils.SettingsXmlUtils;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author U539509
 */
public class SettingsWindowController implements Initializable {
	
	@FXML
	Button saveButton;
	@FXML
	Button CancelButton;
	
	//ftp settings
	@FXML
	TextField ftpServer;
	@FXML
	TextField ftpLogin;
	@FXML
	PasswordField ftpPassword;
	@FXML
	TextField pigeonsDirectory;
	@FXML
	TextField flightsDirectory;
	
	//beacons settings
	@FXML
	Button addBeaconButton;
	@FXML
	Button deleteBeaconButton;
	@FXML
	Button editBeaconButton;
	@FXML
	ListView<String> beaconList;
	
	//other settings
	@FXML
	TextField pigeonHouseLocation;
	@FXML
	Button pigeonHouseLocationMap;
	
	
	Stage dialogStage;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    	//set settings values
        ftpServer.setText(JGolebieV2.settings.getFtpAddress());
        ftpLogin.setText(JGolebieV2.settings.getFtpLogin());
        ftpPassword.setText(JGolebieV2.settings.getFtpPassword());
        pigeonsDirectory.setText(JGolebieV2.settings.getPigeonsDirectory());
        flightsDirectory.setText(JGolebieV2.settings.getFlightsDirectory());
        
        pigeonHouseLocation.setText(JGolebieV2.settings.getPigeonHouseGeoLocation());
        
        editBeaconButton.disableProperty().set(true);
        deleteBeaconButton.disableProperty().set(true);
        
        beaconList.getItems().addAll(JGolebieV2.settings.getBeacons());
		beaconList.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> beaconSelectionChanged(newValue));
    }        
    
    @FXML
    private void onSaveButtonClicked() {
    	//get new settings values
    	JGolebieV2.settings.setFtpAddress(ftpServer.getText());
    	JGolebieV2.settings.setFtpLogin(ftpLogin.getText());
    	JGolebieV2.settings.setFtpPassword(ftpPassword.getText());
    	JGolebieV2.settings.setPigeonsDirectory(pigeonsDirectory.getText());
    	JGolebieV2.settings.setFlightsDirectory(flightsDirectory.getText());
    	
    	JGolebieV2.settings.setPigeonHouseGeoLocation(pigeonHouseLocation.getText());
    	JGolebieV2.settings.setBeacons(beaconList.getItems());
    	//save new settings to file
    	SettingsXmlUtils.saveSettingsToFile(JGolebieV2.settings);
    	
    	dialogStage.close();
    }
    
    @FXML
    private void onCancelButtonClicked() {
    	dialogStage.close();
    }
    
    @FXML
    private void onPigeonHouseLocationMapClicked() {
		// Load the fxml file and create a new stage for the popup dialog.
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("/app/views/MapWindow.fxml"));
		try {
			AnchorPane page = (AnchorPane) loader.load();
			// Create the dialog Stage.
			Stage dialogStage = new Stage();
			dialogStage.setTitle("Wybierz lokalizacje");
			dialogStage.initModality(Modality.WINDOW_MODAL);
			dialogStage.initOwner(this.dialogStage);
			Scene scene = new Scene(page);
			dialogStage.setScene(scene);
	
			MapWindowController controller = loader.getController();
			controller.setStage(dialogStage);
			controller.setStartLocation(pigeonHouseLocation.getText());
			dialogStage.showAndWait();   
			if(controller.isConfirmed()) {
				pigeonHouseLocation.setText(controller.getStartLocationName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    @FXML
    private void onAddBeaconButtonClicked() {
    	TextInputDialog dialog = new TextInputDialog("");
    	dialog.setTitle("Dodaj antenę");
    	dialog.setHeaderText("Wprowadź numer anteny:");
    	dialog.setContentText(null);

    	// Traditional way to get the response value.
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()){
    	    beaconList.getItems().add(result.get());
    	}    	
    }
    
    @FXML
    private void onDeleteBeaconButtonClicked() {
    	String selectedBeacon = beaconList.getSelectionModel().getSelectedItem();
    	
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Potwierdzenie");
		alert.setHeaderText("Czy napewno usunąć zaznaczoną antenę ?");
		alert.setContentText(selectedBeacon);

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK){
			beaconList.getItems().remove(selectedBeacon);
		}
    }
    
    @FXML
    private void onEditBeaconButtonClicked() {
    	String selectedBeacon = beaconList.getSelectionModel().getSelectedItem();
    	
    	TextInputDialog dialog = new TextInputDialog(selectedBeacon);
    	dialog.setTitle("Edycja anteny");
    	dialog.setHeaderText("Wprowadź numer anteny:");
    	dialog.setContentText(null);

    	// Traditional way to get the response value.
    	Optional<String> result = dialog.showAndWait();
    	if (result.isPresent()){
    		int selectedPosition = beaconList.getSelectionModel().getSelectedIndex();
    		beaconList.getItems().set(selectedPosition, result.get());
    	}    
    }
    
    public void setDialogStage(Stage stage) {
    	this.dialogStage = stage;
    }
    
    private void beaconSelectionChanged(String newValue) {
    	if(newValue != null) {
	    	if(!newValue.isEmpty()) {
	    		editBeaconButton.disableProperty().set(false);
	    		deleteBeaconButton.disableProperty().set(false);
	    	}
    	}
    }
}
