package app.controllers;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.connector.serial.SerialPortReader;
import app.connector.serial.SerialPortState;
import app.main.JGolebieV2;
import app.models.Flight;
import app.models.FlightPigeon;
import app.models.Pidgeon;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;

public class PigeonsForFlightWindowController implements Initializable {
	
	private static final Logger log = LogManager.getLogger(PigeonsForFlightWindowController.class.getName()); 
	
	@FXML
	Button startButton;
	@FXML
	Button stopButton;
	@FXML
	Button deleteButton;
	
	//saved pigeons table
	@FXML
	TableView<FlightPigeon> savedTable;
	@FXML
	TableColumn<FlightPigeon, Integer> savedId;
	@FXML
	TableColumn<FlightPigeon, String> savedNumber;
	@FXML
	TableColumn<FlightPigeon, String> savedTime;
	
	//not saved table
	@FXML
	TableView<FlightPigeon> notSavedTable;
	@FXML
	TableColumn<FlightPigeon, Integer> notSavedId;
	@FXML
	TableColumn<FlightPigeon, String> notSavedNumber;
	@FXML
	TableColumn<FlightPigeon, String> notSavedReason;
	
	private SerialPortReader serialPort = null;
	private Flight flight = null;
	private List<Pidgeon> pigeons = null;
	private ObservableList<FlightPigeon> savedPigeons = FXCollections.observableArrayList();
	private ObservableList<FlightPigeon> notSavedPigeons = FXCollections.observableArrayList();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		stopButton.disableProperty().setValue(true);
		deleteButton.disableProperty().setValue(true);
		
		//initialize saved pigeons table
		savedId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
		savedNumber.setCellValueFactory(cellData -> cellData.getValue().pigeonNumberProperty());
		savedTime.setCellValueFactory(cellData -> cellData.getValue().timeProperty());
		savedTable.setItems(savedPigeons);
		
		//initialize not saved pigeons table
		notSavedId.setCellValueFactory(cellData -> cellData.getValue().idProperty().asObject());
		notSavedNumber.setCellValueFactory(cellData -> cellData.getValue().pigeonNumberProperty());
		notSavedReason.setCellValueFactory(cellData -> cellData.getValue().descriptionProperty());
		notSavedTable.setItems(notSavedPigeons);
	}
	
	@FXML
	private void onStartButtonClicked() {
		if(serialPort != null) {
			if(serialPort.isConnected()) {
				//start continous readout on serial port
				serialPort.startContinousRead();
				stopButton.disableProperty().set(false);
				startButton.disableProperty().set(true);
			}
		}
	}
	
	@FXML
	private void onStopButtonClicked() {
		if(serialPort != null) {
			if(serialPort.isConnected()) {
				serialPort.stopContinousRead();
				stopButton.disableProperty().set(true);
				startButton.disableProperty().set(false);
			}
		}
	}
	
	@FXML
	private void onDeleteButtonClicked() {
		
	}
	
	public void setSerialPort(SerialPortReader port) {
		serialPort = port;
		ChangeListener<SerialPortState> stateChangeListener = new ChangeListener<SerialPortState>() {
			@Override
			public void changed(
					ObservableValue<? extends SerialPortState> observable,
					SerialPortState oldValue, SerialPortState newValue) {
				if(newValue == SerialPortState.READ_STARTED) {
					log.info("Reading data from " + JGolebieV2.settings.getBeacons().get(serialPort.beaconIndexProperty().getValue()));
				} else if(newValue == SerialPortState.READ_TIMEOUT) {
					//TODO
				} else if(newValue == SerialPortState.READ_FINISHED) {
					//new ring was read from beacon
					String data = serialPort.dataProperty().getValue();
					String beacon = data.split(";")[0];
					String ring = data.split(";")[1];
					String time = data.split(";")[2];
					String pigeonNumber = "";
					//find pigeon number based on ring id
					for(int i=0; i<pigeons.size(); i++) {
						if(pigeons.get(i).getRing().equals(ring)) {
							pigeonNumber = pigeons.get(i).getNumber();
							break;
						}
					}
					if(pigeonNumber.isEmpty()) { 
						Alert alert = new Alert(AlertType.ERROR);
						alert.getDialogPane().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
						alert.getDialogPane().setPrefWidth(600);
						alert.setTitle("Błąd");
						alert.setHeaderText("Nie znaleziono numeru gołębia");
						alert.setContentText("Nie udało się odnaleźć numeru gołębia powiązanego z obrączką o numerze " + ring);
						alert.showAndWait();
					} else {
						//check if pigeon number is not doubling
						boolean numberOnList = false;
						for(int i=0; i<savedPigeons.size(); i++) {
							if(savedPigeons.get(i).getPigeonNumber().equals(pigeonNumber)) {
								numberOnList = true;
								break;
							}
						}
						if(!numberOnList) {
							for(int i=0; i<notSavedPigeons.size(); i++) {
								if(notSavedPigeons.get(i).getPigeonNumber().equals(pigeonNumber)) {
									numberOnList = true;
									break;
								}
							}
						}
						if(numberOnList) {
							Alert alert = new Alert(AlertType.WARNING);
							alert.getDialogPane().getStylesheets().add(getClass().getResource("/fontSize.css").toString());
							alert.getDialogPane().setPrefWidth(600);
							alert.setTitle("Ostrzeżenie");
							alert.setHeaderText("Gołąb został już zakoszowany");
							alert.setContentText("Gołąb o numerze " + pigeonNumber + " został już zakoszowany");
							alert.showAndWait();
						} else {
							//add new pigeon to saved table
							FlightPigeon newPigeon = new FlightPigeon(savedPigeons.size()+1,pigeonNumber,ring,time,"");
							savedPigeons.add(newPigeon);
							savedTable.getSelectionModel().select(newPigeon);	
							savedTable.scrollTo(newPigeon);
							
							savedId.setText("Id (" + savedPigeons.size() + ")");	
						}
					}
				}
			}
		};
		serialPort.stateProperty().addListener(stateChangeListener);
	}
	
	public void setFlight(Flight flight) {
		this.flight = flight;
		savedPigeons.addAll(flight.getPigeonsSaved());
		savedId.setText("Id (" + savedPigeons.size() + ")");
		notSavedPigeons.addAll(flight.getPigeonsNotSaved());
		notSavedId.setText("Id (" + notSavedPigeons.size() + ")");
	}
	
	public List<FlightPigeon> getSavedPigeons() {
		return savedPigeons;
	}
	
	public List<FlightPigeon> getNotSavedPigeons() {
		return notSavedPigeons;
	}
	
	public void setPigeons(List<Pidgeon> pigeons) {
		this.pigeons = pigeons;
	}
}
