/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.connector.serial;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import app.main.JGolebieV2;
import app.utils.ExceptionUtils;

/**
 *
 * @author U539509
 */
public class SerialPortReader{
	
	private static final Logger log = LogManager.getLogger(SerialPortReader.class.getName()); 

	private SerialPort serialPort;
	private int baudRate = SerialPort.BAUDRATE_9600;
	private int dataBits = SerialPort.DATABITS_8;
	private int parity = SerialPort.PARITY_NONE;
	private int stopBits = SerialPort.STOPBITS_1;
	private String tempData = "";
	private int timeout = 400;
	
	private Timer timeoutTimer;
	
	private BooleanProperty connected = new SimpleBooleanProperty(false);
	private StringProperty data = new SimpleStringProperty("");
	private IntegerProperty  beaconIndex = new SimpleIntegerProperty(0);
	private ObservableList<String> dataList = FXCollections.observableArrayList();
	private ObjectProperty<SerialPortState> state = new SimpleObjectProperty<SerialPortState>(SerialPortState.DISCONNECTED);
	private ObjectProperty<SerialPortReadType> readType = new SimpleObjectProperty<SerialPortReadType>(SerialPortReadType.DEFAULT);
	
	public SerialPortReader() {
		serialPort = new SerialPort("");
	}
	
	public SerialPortReader(String portName) {
		serialPort = new SerialPort(portName);
	}
	
	public boolean open() throws SerialPortException {
		if(!serialPort.isOpened()) {
			log.info("Serial port " + serialPort.getPortName() + " opened");
			connected.setValue(serialPort.openPort());
			state.setValue(SerialPortState.CONNECTED);
			serialPort.setParams(baudRate, dataBits, stopBits, parity);
			serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
			serialPort.setEventsMask(SerialPort.MASK_RXCHAR);			
			serialPort.addEventListener(new SerialPortEventListener() {
				@Override
				public void serialEvent(SerialPortEvent event) {
					if (event.isRXCHAR()) {// If data is available
						while (event.getEventValue() != 0) {
							if (connected.getValue()) {
								try {
									String data = serialPort.readString(1);
									Platform.runLater(new Runnable() {
										@Override
										public void run() {
											processSerialData(data);
										}
									});
								} catch (SerialPortException e) {
									log.error(e);
								}
							} else {
								break;
							}
						}
					}
				}
			});
		}
		else {
			connected.setValue(false);
			state.setValue(SerialPortState.DISCONNECTED);
		}
		return connected.getValue();
	}
	
	public boolean close() throws SerialPortException { 
		connected.setValue(false);
		state.setValue(SerialPortState.DISCONNECTED);
		readType.setValue(SerialPortReadType.DEFAULT);
		log.info("Serial port closed");
		if(serialPort.isOpened()) {
			return serialPort.closePort();
		}
		else return false;
	}
	
	public void readDataFromAllBeacons() {
		log.info("Reading data from all beacons...");
		readType.setValue(SerialPortReadType.READ_ALL_BEACONS);
		readNextBeacon();
	}
	
	public void startContinousRead() {
		log.info("Starting continous read from all beacons...");
		readType.setValue(SerialPortReadType.READ_CONTINOUS);
		readNextBeacon();
	}
	
	public void stopContinousRead() {
		log.info("Stopping continous read from all beacons...");
		readType.setValue(SerialPortReadType.DEFAULT);
	}
	
	public void readDataFromBeacon(String beacon) {
		try {
			log.info("Reading data from beacon " + beacon);
			readType.setValue(SerialPortReadType.READ_ONE_BEACON);
			state.setValue(SerialPortState.READ_STARTED);			
			
			serialPort.writeString(":?" + beacon + "ID$0A\n\r");
			log.info("Serial write: " + ":?" + beacon + "ID$0A");
			timeoutTimer = new Timer(true);
			timeoutTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					// timeout occured
					log.warn("Serial port timeout");
					state.setValue(SerialPortState.READ_TIMEOUT);
					readType.setValue(SerialPortReadType.DEFAULT);
				}
			}, timeout);
		} catch (SerialPortException e) {
			log.error(e);
			ExceptionUtils.showExceptionDialog(e);
		}	
	}

	private void readNextBeacon() {
        try {
			state.setValue(SerialPortState.READ_STARTED);			
        	
			serialPort.writeString(":?" + JGolebieV2.settings.getBeacons().get(beaconIndex.getValue()) + "ID$0A\n\r");
			log.info("Serial write: " + ":?" + JGolebieV2.settings.getBeacons().get(beaconIndex.getValue()) + "ID$0A");
            timeoutTimer = new Timer("TimeoutTimer",true);
            timeoutTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                	//timeout occured
        			log.warn("Serial port timeout");
                	state.setValue(SerialPortState.READ_TIMEOUT);
                	//set counter to next beacon from the list
                	int beaconCount = JGolebieV2.settings.getBeacons().size();
                	if(readType.getValue() == SerialPortReadType.READ_CONTINOUS) {
	                	if(beaconIndex.getValue() == beaconCount-1) {
	                		beaconIndex.setValue(0);
	                	}else {
	                		beaconIndex.setValue(beaconIndex.getValue() + 1);
	                	}
	                	//try to read next beacon
	                	readNextBeacon();
                	}else if(readType.getValue() == SerialPortReadType.READ_ALL_BEACONS) {
                		if(beaconIndex.getValue() < beaconCount-1) {
                			beaconIndex.setValue(beaconIndex.getValue() + 1);
                			//try to read next beacon
                			readNextBeacon();
                		}else {
                			beaconIndex.setValue(0);
                			readType.setValue(SerialPortReadType.DEFAULT);
                			log.info("Data read finished");
                		}
                	}
                }
            }, timeout);			
		} catch (SerialPortException e) {
			log.error(e);
			ExceptionUtils.showExceptionDialog(e);
		}
	}

	private void processSerialData(String data) {
		tempData += data;
		if (tempData.startsWith("$") && tempData.endsWith("\n\r")) {
			if(tempData.contains("EMPTY")) {
				//no rings in beacon buffer
				log.info("Serial read: " + tempData.trim());
				tempData = "";
			}
			else if (tempData.length() > 30) {
				//stop timeout timers
				if(timeoutTimer != null) {
					timeoutTimer.cancel();
					timeoutTimer = null;
				}
				tempData = tempData.trim();
				log.info("Serial received: " + tempData);				
				String ringId = tempData.split("ID")[1].split("T")[0].trim().substring(0, 8);
				String timeStamp = tempData.split("ID")[1].split("T")[1].trim();
				String beaconId = tempData.split("ID")[0];

				DateTimeFormatter formatter = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss.SSS");
				LocalDateTime time = formatter.parseLocalDateTime(new LocalDateTime().toString(formatter));
				LocalDateTime timeAfter = formatter.parseLocalDateTime(new LocalDateTime().toString(formatter));
				timeAfter = time.minusMillis(Integer.parseInt(timeStamp));
				
				log.info("Data processed, beaconId=" + beaconId + ", ringId=" +ringId + ", timeStamp=" + timeAfter.toString(formatter));
				
				//TODO: set data property and add ring info to data list
				this.data.setValue(beaconId + ";" + ringId + ";" + timeAfter.toString(formatter));
				//read finished
				state.setValue(SerialPortState.READ_FINISHED);

				if(readType.getValue() == SerialPortReadType.READ_CONTINOUS) {
                	int beaconCount = JGolebieV2.settings.getBeacons().size();
                	if(beaconIndex.getValue() == beaconCount-1) {
                		beaconIndex.setValue(0);
                	}else {
                		beaconIndex.setValue(beaconIndex.getValue() + 1);
                	}
                	//try to read next beacon
					readNextBeacon();
				}
			}
			tempData = "";
		}
		if (!tempData.startsWith("$")) {
			tempData = "";
		}

	}

	public final BooleanProperty connectedProperty() {
		return this.connected;
	}

	public final boolean isConnected() {
		return this.connectedProperty().get();
	}

	public final StringProperty dataProperty() {
		return this.data;
	}

	public final IntegerProperty beaconIndexProperty() {
		return this.beaconIndex;
	}
	
	public final ObjectProperty<SerialPortState> stateProperty() {
		return this.state;
	}
	
	public ObservableList<String> getDataList() {
		return this.dataList;
	}

	public final ObjectProperty<SerialPortReadType> readTypeProperty() {
		return this.readType;
	}
}
