package app.connector.serial;

public enum SerialPortReadType {
	
	DEFAULT(0,"Default state. No read operation started"),
	READ_ONE_BEACON(1,"Reads data from one beacon and then stops"),
	READ_ALL_BEACONS(2,"Reads data from all beacons and then stops. Also stops after first ring was read"),
	READ_CONTINOUS(3,"Reads data from all beacons indefinetly");
	
	private int id;
	private String description;
	
	private SerialPortReadType(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}

}
