package app.connector.serial;

public enum SerialPortState {
	
	DEFAULT(0,"Default state"),
	DISCONNECTED(1,"Serial port is not connected"),
	CONNECTED(2,"Serial port is connected"),
	READ_STARTED(3,"Write message was send to serial port to read beacon data"),
	READ_TIMEOUT(4,"Didn't receive response from beacon"),
	READ_FINISHED(5,"Response was received"),
	READ_ERROR(6,"There was problem with processing the received data");

	private final int id;
	private final String description;
	
	private SerialPortState(int id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public int getId() {
		return id;
	}
	
	public String getDescription() {
		return description;
	}
}