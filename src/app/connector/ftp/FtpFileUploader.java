package app.connector.ftp;

import it.sauronsoftware.ftp4j.FTPClient;

import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import app.connector.serial.SerialPortReader;
import app.main.JGolebieV2;
import app.utils.ExceptionUtils;

public class FtpFileUploader {
	
	private static final Logger log = LogManager.getLogger(FtpFileUploader.class.getName()); 
	
	public static BooleanProperty fileSend = new SimpleBooleanProperty();
	public static BooleanProperty fileTransferStarted = new SimpleBooleanProperty();
	public static BooleanProperty fileTransferError = new SimpleBooleanProperty();
	
	/**
	 * @brief Uploads given file to remote ftp server in separate thread.
	 * @param filePath Path to file which needs to be uploaded. File path is relative to application directory
	 */
	public static void uploadFileInBackground(String filePath) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				fileTransferStarted.setValue(true);
				boolean result = uploadFile(filePath);
				if(result) {
					fileSend.setValue(true);
				}else {
					fileSend.setValue(false);
				}
				fileTransferStarted.setValue(false);
			}

		}).start();
	}

	/**
	 * @brief Uploads given file to remote ftp server in main thread. 
	 * @param filePath Path to file which needs to be uploaded. File path is relative to application directory
	 * @return True if file upload was successful, otherwise false
	 */
	public static boolean uploadFile(String filePath) {
		return uploadFile(filePath,"/");
	}
	
	/**
	 * @brief Uploads given file to remote ftp server in main thread. 
	 * @param filePath Path to file which needs to be uploaded. File path is relative to application directory
	 * @param remotePath Path on the remote server where file will be uploaded
	 * @return True if file upload was successful, otherwise false
	 */
	public static boolean uploadFile(String filePath, String remotePath) {
		FTPClient client = new FTPClient();
		if (!JGolebieV2.settings.getFtpAddress().isEmpty()
				&& !JGolebieV2.settings.getFtpLogin().isEmpty()
				&& !JGolebieV2.settings.getFtpPassword().isEmpty()) {

				try {
					log.info("File transfer begin, connecting with " + JGolebieV2.settings.getFtpAddress());					
					client.connect(JGolebieV2.settings.getFtpAddress(),21);
					client.login(JGolebieV2.settings.getFtpLogin(),JGolebieV2.settings.getFtpPassword());
					
					client.changeDirectory(remotePath);
					client.setType(FTPClient.TYPE_TEXTUAL);
					log.info("Remote path set to: " + client.currentDirectory(), ", starting file upload");
					
		            String appDir = System.getProperty("user.dir");
					log.info("Uploading " + filePath);
		            File file = new File(appDir + filePath);
		            if(file.exists()) {
		            	client.upload(file);
		            }
					
					//file upload ok
		            log.info("File upload ok, disconnecting...");
					client.disconnect(true);
					fileTransferError.setValue(false);
					return true;
				} catch (Exception e) {
					log.error(e);
					fileTransferError.setValue(true);
					return false;
				}
		}
		
		//no credentials found
		fileTransferError.setValue(true);
		return false;
	}
	
	/**
	 * @brief Uploads multiple files to remote ftp server in main thread.
	 * @param files Array of file paths which needs to be uploaded. File paths are relative to application directory
	 * @return True if all files were uploaded successful, otherwise false
	 */
	public static boolean uploadMultipleFiles(String files[]) {
		FTPClient client = new FTPClient();
		if (!JGolebieV2.settings.getFtpAddress().isEmpty()
				&& !JGolebieV2.settings.getFtpLogin().isEmpty()
				&& !JGolebieV2.settings.getFtpPassword().isEmpty()) {

				try {
					log.info("File transfer begin, connecting with " + JGolebieV2.settings.getFtpAddress());					
					client.connect(JGolebieV2.settings.getFtpAddress(),21);
					client.login(JGolebieV2.settings.getFtpLogin(),JGolebieV2.settings.getFtpPassword());
					log.info("Remote path set to: " + client.currentDirectory(), ", starting file upload");
					
					for(String filePath : files) {
						log.info("Uploading " + filePath);
			            String appDir = System.getProperty("user.dir");
			            File file = new File(appDir + filePath);
			            if(file.exists()) {
			            	client.upload(file);
			            }
					}
					
					//file upload ok
					log.info("All files uploaded, disconnecting...");
					client.disconnect(true);
					fileTransferError.setValue(false);
					return true;
				} catch (Exception e) {
					log.error(e);
					fileTransferError.setValue(true);
					return false;
				}
		}
		
		//no credentials found
		fileTransferError.setValue(true);
		return false;
	}
	
	/**
	 * @brief Uploads all data files from /data directory to remote ftp server. File upload is executed in separate thread
	 */
	public static void uploadDataFiles() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				log.info("Uploading all data files...");
				fileTransferStarted.setValue(true);
				if(!JGolebieV2.settings.getPigeonsDirectory().isEmpty()) {
					 uploadFile("/data/pigeons.xml",JGolebieV2.settings.getPigeonsDirectory());
				} else{ 
					uploadFile("/data/pigeons.xml");
				}
				if(!JGolebieV2.settings.getFlightsDirectory().isEmpty()) {
					uploadFile("/data/flights.xml",JGolebieV2.settings.getFlightsDirectory());
				} else {
					uploadFile("/data/flights.xml");
				}
				fileTransferStarted.setValue(false);
			}
		}).start();
	}
}
