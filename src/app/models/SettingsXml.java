package app.models;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "settings")
public class SettingsXml {
	
	private Settings settings;

	public Settings getSettings() {
		return settings;
	}

	@XmlElement(name = "setting")
	public void setSettings(Settings settings) {
		this.settings = settings;
	}
}
