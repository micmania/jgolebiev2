package app.models;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "flights")
public class FlightXml {
	private List<Flight> flights;

	public List<Flight> getFlights() {
        return flights;
    }
    
    @XmlElement(name = "flight")
    public void setFlights(List<Flight> flights) {
        this.flights = flights;
    }
}
