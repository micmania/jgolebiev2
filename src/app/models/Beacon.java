package app.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Beacon {
	
	private StringProperty number;
	private StringProperty port;
	
	public Beacon() {
		this.number = new SimpleStringProperty("");
		this.port = new SimpleStringProperty("");
	}
	
	
	
	public Beacon(String number, String port) {
		super();
		this.number = new SimpleStringProperty(number);
		this.port = new SimpleStringProperty(port);
	}

	@Override
	public String toString() {
		return "Beacon [number=" + number + ", port=" + port + "]";
	}

	public final StringProperty numberProperty() {
		return this.number;
	}
	public final java.lang.String getNumber() {
		return this.numberProperty().get();
	}
	public final void setNumber(final java.lang.String number) {
		this.numberProperty().set(number);
	}
	public final StringProperty portProperty() {
		return this.port;
	}
	public final java.lang.String getPort() {
		return this.portProperty().get();
	}
	public final void setPort(final java.lang.String port) {
		this.portProperty().set(port);
	}

}
