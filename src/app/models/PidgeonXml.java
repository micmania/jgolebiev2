/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package app.models;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Michal
 */
@XmlRootElement(name = "pidgeons")
public class PidgeonXml {
    
    private List<Pidgeon> pidgeons;
    
    public List<Pidgeon> getPidgeons() {
        return pidgeons;
    }
    
    @XmlElement(name = "pidgeon")
    public void setPidgeons(List<Pidgeon> pidgeons) {
        this.pidgeons = pidgeons;
    }
}
