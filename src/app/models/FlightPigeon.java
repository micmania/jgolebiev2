package app.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FlightPigeon {
	private IntegerProperty id;
	private StringProperty pigeonNumber;
	private StringProperty ringNumber;
	private StringProperty time;
	private StringProperty description;
	
	public FlightPigeon() {
		this.id = new SimpleIntegerProperty(0);
		this.pigeonNumber = new SimpleStringProperty("");
		this.ringNumber = new SimpleStringProperty("");
		this.time = new SimpleStringProperty("");
		this.description = new SimpleStringProperty("");
	}
	
	public FlightPigeon(int id, String pigeonNumber,
			String ringNumber, String time, String description) {
		super();
		this.id = new SimpleIntegerProperty(id);
		this.pigeonNumber = new SimpleStringProperty(pigeonNumber);
		this.ringNumber = new SimpleStringProperty(ringNumber);
		this.time = new SimpleStringProperty(time);
		this.description = new SimpleStringProperty(description);
	}

	@Override
	public String toString() {
		return "FlightPigeon [id=" + id + ", pigeonNumber=" + pigeonNumber
				+ ", ringNumber=" + ringNumber + ", time=" + time
				+ ", description=" + description + "]";
	}

	public final IntegerProperty idProperty() {
		return this.id;
	}
	public final int getId() {
		return this.idProperty().get();
	}
	public final void setId(final int id) {
		this.idProperty().set(id);
	}
	public final StringProperty pigeonNumberProperty() {
		return this.pigeonNumber;
	}
	public final java.lang.String getPigeonNumber() {
		return this.pigeonNumberProperty().get();
	}
	public final void setPigeonNumber(final java.lang.String pigeonNumber) {
		this.pigeonNumberProperty().set(pigeonNumber);
	}
	public final StringProperty ringNumberProperty() {
		return this.ringNumber;
	}
	public final java.lang.String getRingNumber() {
		return this.ringNumberProperty().get();
	}
	public final void setRingNumber(final java.lang.String ringNumber) {
		this.ringNumberProperty().set(ringNumber);
	}
	public final StringProperty timeProperty() {
		return this.time;
	}
	public final java.lang.String getTime() {
		return this.timeProperty().get();
	}
	public final void setTime(final java.lang.String time) {
		this.timeProperty().set(time);
	}

	public final StringProperty descriptionProperty() {
		return this.description;
	}

	public final java.lang.String getDescription() {
		return this.descriptionProperty().get();
	}

	public final void setDescription(final java.lang.String description) {
		this.descriptionProperty().set(description);
	}
}
