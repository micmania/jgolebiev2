/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.models;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author U539509
 */
public class Pidgeon {
    private final IntegerProperty id;
    private final StringProperty number;
    private final StringProperty team;
    private final StringProperty sex;
    private final StringProperty ring;
    private final StringProperty color;
    private final StringProperty description;
    
    
    public Pidgeon(int id, String number, String team, String sex,String color, String ring, String description) {
        this.id = new SimpleIntegerProperty(id);
        this.number = new SimpleStringProperty(number);
        this.team = new SimpleStringProperty(team);
        this.sex = new SimpleStringProperty(sex);
        this.ring = new SimpleStringProperty(ring);
        this.color = new SimpleStringProperty(color);
        this.description = new SimpleStringProperty(description);
    }
    
    public Pidgeon() {
        this.id = new SimpleIntegerProperty();
        this.number = new SimpleStringProperty();
        this.team = new SimpleStringProperty();
        this.sex = new SimpleStringProperty();
        this.ring = new SimpleStringProperty();
        this.color = new SimpleStringProperty();
        this.description = new SimpleStringProperty("");
    }

    @Override
	public String toString() {
		return "Pidgeon [id=" + id + ", number=" + number + ", team=" + team
				+ ", sex=" + sex + ", ring=" + ring + ", color=" + color + "]";
	}
    
    @Override 
    public boolean equals(Object other) {
    	if(other != null) {
	    	Pidgeon pigeon = (Pidgeon)other;
	    	if(pigeon.getId() == this.getId() &&
	    	   pigeon.getNumber().equals(this.getNumber()) &&
	    	   pigeon.getRing().equals(this.getRing()) &&
	    	   pigeon.getColor().equals(this.getColor()) &&
	    	   pigeon.getSex().equals(this.getSex()) &&
	    	   pigeon.getDescription().equals(this.getDescription())) {
	    		//TODO: equalise getDescription
	    		return true;
	    	}
	    	return false;
    	}
    	return false;
    }

	public int getId() {
        return id.get();
    }

    public void setId(int value) {
        id.set(value);
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public String getNumber() {
        return number.get();
    }

    public void setNumber(String value) {
        number.set(value);
    }

    public StringProperty numberProperty() {
        return number;
    }

    public String getTeam() {
        return team.get();
    }

    public void setTeam(String value) {
        team.set(value);
    }

    public StringProperty teamProperty() {
        return team;
    }
    
    public String getSex() {
        return sex.get();
    }
    
    public void setSex(String value) {
        sex.set(value);
    }
    
    public StringProperty sexProperty() {
        return sex;
    }

	public final StringProperty ringProperty() {
		return this.ring;
	}

	public final java.lang.String getRing() {
		return this.ringProperty().get();
	}

	public final void setRing(final java.lang.String ring) {
		this.ringProperty().set(ring);
	}

	public final StringProperty colorProperty() {
		return this.color;
	}

	public final java.lang.String getColor() {
		return this.colorProperty().get();
	}

	public final void setColor(final java.lang.String color) {
		this.colorProperty().set(color);
	}

	public final StringProperty descriptionProperty() {
		return this.description;
	}

	public final java.lang.String getDescription() {
		return this.descriptionProperty().get();
	}

	public final void setDescription(final java.lang.String description) {
		this.descriptionProperty().set(description);
	}
}
