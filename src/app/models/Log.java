package app.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Log {
	private final StringProperty date;
	private final StringProperty type;
	private final StringProperty message;
	
	public Log() {
		date = new SimpleStringProperty();
		type = new SimpleStringProperty();
		message = new SimpleStringProperty();
	}
	
	public final StringProperty dateProperty() {
		return this.date;
	}
	public final java.lang.String getDate() {
		return this.dateProperty().get();
	}
	public final void setDate(final java.lang.String date) {
		this.dateProperty().set(date);
	}
	public final StringProperty typeProperty() {
		return this.type;
	}
	public final java.lang.String getType() {
		return this.typeProperty().get();
	}
	public final void setType(final java.lang.String type) {
		this.typeProperty().set(type);
	}
	public final StringProperty messageProperty() {
		return this.message;
	}
	public final java.lang.String getMessage() {
		return this.messageProperty().get();
	}
	public final void setMessage(final java.lang.String message) {
		this.messageProperty().set(message);
	}
}
