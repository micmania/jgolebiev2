package app.models;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.sun.xml.internal.txw2.annotation.XmlElement;

import app.utils.LocalDateXmlAdapter;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Flight {
	private final IntegerProperty id;
	private final StringProperty name;
	private final StringProperty place;
	private final DoubleProperty distance;
	private final ObjectProperty<LocalDate> date;
	private final StringProperty hour;
	private final ObjectProperty<List<FlightPigeon>> pigeonsSaved;
	private final ObjectProperty<List<FlightPigeon>> pigeonsNotSaved;

	
	public Flight() {
		this.id = new SimpleIntegerProperty(0);
		this.name = new SimpleStringProperty("");
		this.place = new SimpleStringProperty("");
		this.distance = new SimpleDoubleProperty(0.0);
		this.date = new SimpleObjectProperty<LocalDate>(null);
		this.hour = new SimpleStringProperty("");
		this.pigeonsSaved = new SimpleObjectProperty<List<FlightPigeon>>(new ArrayList<>());
		this.pigeonsNotSaved = new SimpleObjectProperty<List<FlightPigeon>>(new ArrayList<>());
	}
	
	public Flight(int id, String name,String place, double distance,LocalDate date, String hour) {
		this.id = new SimpleIntegerProperty(id);
		this.name = new SimpleStringProperty(name);
		this.place = new SimpleStringProperty(place);
		this.distance = new SimpleDoubleProperty(distance);
		this.date = new SimpleObjectProperty<LocalDate>(date);
		this.hour = new SimpleStringProperty(hour);
		this.pigeonsSaved = new SimpleObjectProperty<List<FlightPigeon>>(new ArrayList<>());
		this.pigeonsNotSaved = new SimpleObjectProperty<List<FlightPigeon>>(new ArrayList<>());
	}
	public final IntegerProperty idProperty() {
		return this.id;
	}
	public final int getId() {
		return this.idProperty().get();
	}
	public final void setId(final int id) {
		this.idProperty().set(id);
	}
	public final StringProperty nameProperty() {
		return this.name;
	}
	public final java.lang.String getName() {
		return this.nameProperty().get();
	}
	public final void setName(final java.lang.String name) {
		this.nameProperty().set(name);
	}
	public final StringProperty placeProperty() {
		return this.place;
	}
	public final java.lang.String getPlace() {
		return this.placeProperty().get();
	}
	public final void setPlace(final java.lang.String place) {
		this.placeProperty().set(place);
	}
	public final DoubleProperty distanceProperty() {
		return this.distance;
	}
	public final double getDistance() {
		return this.distanceProperty().get();
	}
	public final void setDistance(final double distance) {
		this.distanceProperty().set(distance);
	}
	public final ObjectProperty<LocalDate> dateProperty() {
		return this.date;
	}
	@XmlJavaTypeAdapter(LocalDateXmlAdapter.class)
	public final java.time.LocalDate getDate() {
		return this.dateProperty().get();
	}
	public final void setDate(final java.time.LocalDate date) {
		this.dateProperty().set(date);
	}
	public final StringProperty hourProperty() {
		return this.hour;
	}
	public final java.lang.String getHour() {
		return this.hourProperty().get();
	}
	public final void setHour(final java.lang.String hour) {
		this.hourProperty().set(hour);
	}
	
	public final ObjectProperty<List<FlightPigeon>> pigeonsSavedProperty() {
		return this.pigeonsSaved;
	}

	public final java.util.List<FlightPigeon> getPigeonsSaved() {
		return this.pigeonsSavedProperty().get();
	}

	public final void setPigeonsSaved(
			final java.util.List<FlightPigeon> pigeonsSaved) {
		this.pigeonsSavedProperty().set(pigeonsSaved);
	}

	public final ObjectProperty<List<FlightPigeon>> pigeonsNotSavedProperty() {
		return this.pigeonsNotSaved;
	}

	public final java.util.List<FlightPigeon> getPigeonsNotSaved() {
		return this.pigeonsNotSavedProperty().get();
	}
	
	public final void setPigeonsNotSaved(
			final java.util.List<FlightPigeon> pigeonsNotSaved) {
		this.pigeonsNotSavedProperty().set(pigeonsNotSaved);
	}
}
