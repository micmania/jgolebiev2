package app.models;

import java.util.ArrayList;
import java.util.List;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Settings {
	private final StringProperty ftpAddress;
	private final StringProperty ftpLogin;
	private final StringProperty ftpPassword;
	private final StringProperty pigeonsDirectory;
	private final StringProperty flightsDirectory;
	private final StringProperty pigeonHouseGeoLocation;
	private final ObjectProperty<List<String>> beacons;

	public Settings() {
		super();
		this.ftpAddress = new SimpleStringProperty("");
		this.ftpLogin = new SimpleStringProperty("");
		this.ftpPassword = new SimpleStringProperty("");
		this.pigeonsDirectory = new SimpleStringProperty("");
		this.flightsDirectory = new SimpleStringProperty("");
		this.pigeonHouseGeoLocation = new SimpleStringProperty("");
		this.beacons = new SimpleObjectProperty<List<String>>(new ArrayList<String>());
	}
	
	public Settings(Settings settings) {
		super();
		this.ftpAddress = new SimpleStringProperty(settings.getFtpAddress());
		this.ftpLogin = new SimpleStringProperty(settings.getFtpLogin());
		this.ftpPassword = new SimpleStringProperty(settings.getFtpPassword());		
		this.pigeonsDirectory = new SimpleStringProperty(settings.getPigeonsDirectory());
		this.flightsDirectory = new SimpleStringProperty(settings.getFlightsDirectory());
		this.pigeonHouseGeoLocation = new SimpleStringProperty(settings.getPigeonHouseGeoLocation());
		this.beacons = new SimpleObjectProperty<List<String>>(settings.getBeacons());
	}
	
	@Override
	public boolean equals(Object other) {
		Settings settings = (Settings)other;
		if(settings.getFtpAddress().equals(this.ftpAddress.getValue()) &&
		   settings.getFtpLogin().equals(this.ftpLogin.getValue()) &&
		   settings.getFtpPassword().equals(this.ftpPassword.getValue()) &&
		   settings.getPigeonsDirectory().equals(this.pigeonsDirectory.getValue()) &&
		   settings.getFlightsDirectory().equals(this.flightsDirectory.getValue()) &&
		   settings.getPigeonHouseGeoLocation().equals(this.pigeonHouseGeoLocation.getValue())) {
			//TODO: compare beacons lists
			return true;
		}
		else 
			return false;
	}
	
	
	@Override
	public String toString() {
		return "Settings [ftpAddress=" + ftpAddress + ", ftpLogin=" + ftpLogin
				+ ", ftpPassword=" + ftpPassword + ", pigeonsDirectory="
				+ pigeonsDirectory + ", flightsDirectory=" + flightsDirectory
				+ ", pigeonHouseGeoLocation=" + pigeonHouseGeoLocation
				+ ", beacons=" + beacons + "]";
	}

	public final StringProperty ftpAddressProperty() {
		return this.ftpAddress;
	}

	public final java.lang.String getFtpAddress() {
		return this.ftpAddressProperty().get();
	}

	public final void setFtpAddress(final java.lang.String ftpAddress) {
		this.ftpAddressProperty().set(ftpAddress);
	}


	public final StringProperty ftpLoginProperty() {
		return this.ftpLogin;
	}


	public final java.lang.String getFtpLogin() {
		return this.ftpLoginProperty().get();
	}


	public final void setFtpLogin(final java.lang.String ftpLogin) {
		this.ftpLoginProperty().set(ftpLogin);
	}


	public final StringProperty ftpPasswordProperty() {
		return this.ftpPassword;
	}


	public final java.lang.String getFtpPassword() {
		return this.ftpPasswordProperty().get();
	}


	public final void setFtpPassword(final java.lang.String ftpPassword) {
		this.ftpPasswordProperty().set(ftpPassword);
	}

	public final StringProperty pigeonsDirectoryProperty() {
		return this.pigeonsDirectory;
	}

	public final java.lang.String getPigeonsDirectory() {
		return this.pigeonsDirectoryProperty().get();
	}

	public final void setPigeonsDirectory(final java.lang.String pigeonsDirectory) {
		this.pigeonsDirectoryProperty().set(pigeonsDirectory);
	}

	public final StringProperty flightsDirectoryProperty() {
		return this.flightsDirectory;
	}

	public final java.lang.String getFlightsDirectory() {
		return this.flightsDirectoryProperty().get();
	}

	public final void setFlightsDirectory(final java.lang.String flightsDirectory) {
		this.flightsDirectoryProperty().set(flightsDirectory);
	}

	public final StringProperty pigeonHouseGeoLocationProperty() {
		return this.pigeonHouseGeoLocation;
	}

	public final java.lang.String getPigeonHouseGeoLocation() {
		return this.pigeonHouseGeoLocationProperty().get();
	}

	public final void setPigeonHouseGeoLocation(
			final java.lang.String pigeonHouseGeoLocation) {
		this.pigeonHouseGeoLocationProperty().set(pigeonHouseGeoLocation);
	}

	public final ObjectProperty<List<String>> beaconsProperty() {
		return this.beacons;
	}

	public final java.util.List<java.lang.String> getBeacons() {
		return this.beaconsProperty().get();
	}

	public final void setBeacons(final java.util.List<java.lang.String> beacons) {
		this.beaconsProperty().set(beacons);
	}
}
