/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app.main;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import app.controllers.MainWindowController;
import app.models.Settings;

/**
 *
 * @author U539509
 */
public class JGolebieV2 extends Application {
	
	private static final Logger log = LogManager.getLogger(JGolebieV2.class.getName()); 
	
	public static Settings settings = new Settings();
    
    @Override
    public void start(Stage stage) throws Exception {
        try {
        	log.info("Application started");
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("/app/views/MainWindow.fxml"));
            Parent root = loader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("/fontSize.css").toString());
            //set window title with launch date and time
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd '@' HH:mm:ss");
            stage.setTitle("JGolebie - " + ft.format(dNow));
            stage.setScene(scene);
            stage.getIcons().add(new Image(getClass().getResourceAsStream("/bird-red-icon.png")));
            
            MainWindowController controller = loader.getController();
            controller.setMainWindowStage(stage);
        	setUserAgentStylesheet(STYLESHEET_MODENA);

            stage.show();   
        }
        catch(IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
