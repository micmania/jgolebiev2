package app.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringEncryptionUtilsTest {
	
	String message;

	@Before
	public void setUp() throws Exception {
		message = "Test message to encrypt";
	}

	@Test
	public void testEncryptAndDecrypt() {
		String encrypted = StringEncryptionUtils.encrypt(message);
		String decrypted = StringEncryptionUtils.decrypt(encrypted);
		assertEquals(message, decrypted);
	}

}
