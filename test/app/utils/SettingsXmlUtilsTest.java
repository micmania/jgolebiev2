package app.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.Before;
import org.junit.Test;

import app.models.Settings;

public class SettingsXmlUtilsTest {

	private Settings settings;
	private final String filePath = "/settingsTest.xml";
	
	@Before
	public void setUp() throws Exception {
		settings = new Settings();
		settings.setFtpAddress("address");
		settings.setFtpLogin("login");
		settings.setFtpPassword("password");
		settings.setPigeonsDirectory("pigeonsDir");
		settings.setFlightsDirectory("flightDir");
		settings.setPigeonHouseGeoLocation("pigeonHouse");
	}
	
	@Test
	public void testSaveAndLoadSettings() {
		testSaveSettingsToFile();
		testLoadSettingsFromFile();
	}

	
	public void testSaveSettingsToFile() {
		SettingsXmlUtils.saveSettingsToFile(settings,filePath);
        String appDir = System.getProperty("user.dir");
        File file = new File(appDir + filePath);
        assertTrue(file.exists());
	}
	
	
	public void testLoadSettingsFromFile() {
		Settings loadedSettings = SettingsXmlUtils.loadSettingsFromFile(filePath);
		assertNotNull(loadedSettings);
		assertEquals(settings, loadedSettings);
		//delete temp settings file
	    String appDir = System.getProperty("user.dir");
	    File file = new File(appDir + filePath);
	    file.delete();	
	}
}
