package app.utils;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;

import app.models.Pidgeon;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PigeonXmlUtilsTest {

	List<Pidgeon> pigeons;
	private final String filePath = "/data/pigeonsTest.xml";
	
	@Before
	public void setUp() throws Exception {
		pigeons = new ArrayList<>();
		Pidgeon pigeonA = new Pidgeon(0,"PL-0000-0000","TEAM_A","M","BLUE","","Opis");
		Pidgeon pigeonB = new Pidgeon(0,"PL-0000-0001","TEAM_A","M","BLUE","","Opis");
		Pidgeon pigeonC = new Pidgeon(0,"PL-0000-0002","TEAM_B","M","BLUE","","Opis");
		pigeons.add(pigeonA);
		pigeons.add(pigeonB);
		pigeons.add(pigeonC);
	}
	
	@Test
	public void testSaveAndLoadPigeons() {
		testSavePigeonsListToFile();
		testLoadPigeonsListFromFile();
	}

	private void testSavePigeonsListToFile() {
		PidgeonXmlUtils.savePigeonsToFile(pigeons, filePath);
        String appDir = System.getProperty("user.dir");
        File file = new File(appDir + filePath);
        assertTrue(file.exists());
	}

	private void testLoadPigeonsListFromFile() {
		List<Pidgeon> loadedPigeons = PidgeonXmlUtils.loadPigeonsFromFile(filePath);
		assertNotNull(loadedPigeons);
		assertEquals(pigeons, loadedPigeons);
		//delete temp pigeons file
        String appDir = System.getProperty("user.dir");
        File file = new File(appDir + filePath);
		file.delete();
		assertFalse(file.exists());		
	}

}
